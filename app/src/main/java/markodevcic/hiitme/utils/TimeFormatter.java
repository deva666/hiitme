package markodevcic.hiitme.utils;

public final class TimeFormatter {

    private TimeFormatter() {
        throw new IllegalStateException("no instances");
    }

    public static String format(int seconds) {
        if (seconds > 60 * 60) {
            return String.format("%02d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60,
                    seconds % 60);
        } else if (seconds == 60 * 60) {
            return "60:00";
        } else {
            return String.format("%02d:%02d", (seconds % 3600) / 60, seconds % 60);
        }
    }
}
