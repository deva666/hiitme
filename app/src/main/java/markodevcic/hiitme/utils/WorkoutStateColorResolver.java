package markodevcic.hiitme.utils;


import android.content.Context;

import markodevcic.hiitme.R;
import markodevcic.hiitme.workout.Workout;

public final class WorkoutStateColorResolver {
    private final int warmupColor;
    private final int workColor;
    private final int restColor;
    private final int cooldownColor;

    public WorkoutStateColorResolver(Context context) {
        warmupColor = context.getResources().getColor(R.color.green);
        workColor = context.getResources().getColor(R.color.dodger_blue);
        restColor = context.getResources().getColor(R.color.purple);
        cooldownColor = context.getResources().getColor(R.color.dark_orange_2);
    }

    public int getColor(String workoutState) {
        switch (workoutState) {
            case Workout.WARMUP_STATE:
                return warmupColor;
            case Workout.REST_STATE:
                return restColor;
            case Workout.WORK_STATE:
                return workColor;
            case Workout.COOLDOWN_STATE:
                return cooldownColor;
            default:
                throw new IllegalStateException("unknown state");
        }
    }
}
