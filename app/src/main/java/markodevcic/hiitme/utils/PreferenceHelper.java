package markodevcic.hiitme.utils;

public final class PreferenceHelper {
	private PreferenceHelper() {
		throw new IllegalAccessError("no instances");
	}

	public static final String PREF_USE_DEFAULT_SOUND = "pref_use_default_sound";
	public static final String PREF_PLAY_TICKS = "pref_play_ticks";
	public static final String PREF_SOUND = "pref_sound";
	public static final String PREF_KEEP_ON_TOP = "pref_keep_on_top";
	public static final String PREF_RESTORE_DB = "pref_restore_db";
	public static final String PREF_BACKUP_DB = "pref_backup_db";
}
