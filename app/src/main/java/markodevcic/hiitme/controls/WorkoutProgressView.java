package markodevcic.hiitme.controls;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import markodevcic.hiitme.R;

public class WorkoutProgressView
		extends View {

	private static final int MAX_PROGRESS_VALUE = 10000;
	private static final String TEXT_PAUSE = "PAUSE";
	private static final String TEXT_RESUME = "RESUME";
	private static final float START_ANGLE = 270f;
	private static final float FULL_CIRCLE = 360f;

	private final Context context;

	private boolean isPaused = false;

	private int progressRoundValue;
	private int progressWorkoutValue;
	private int strokeColor;
	private float timeTextSize = 350f;

	private float pauseTextSize;
	private float strokeWidth;

	private final RectF progressRect = new RectF();

	private String timeText = "";

	private TextPaint timeTextPaint;

	private TextPaint pauseTextPaint;
	private Paint circleFillPaint;

	private Paint circleBackgroundPaint;
	private final Paint circleStrokePaint = new Paint();
	private final Rect textTimeBounds = new Rect();

	private final Rect textPauseBounds = new Rect();
	private ValueAnimator animator;

	public WorkoutProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.context = context;
		obtainAttrs(context, attrs);
	}

	public WorkoutProgressView(Context context) {
		super(context);
		this.context = context;
	}

	public WorkoutProgressView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		obtainAttrs(context, attrs);
	}

	private void obtainAttrs(Context context, AttributeSet attrs) {
		TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WorkoutProgressView, 0, 0);
		try {
			timeText = typedArray.getString(R.styleable.WorkoutProgressView_timeText) == null ? "" : typedArray.getString(R.styleable.WorkoutProgressView_timeText);
			progressRoundValue = typedArray.getInt(R.styleable.WorkoutProgressView_progressRoundValue, 0);
			progressWorkoutValue = typedArray.getInt(R.styleable.WorkoutProgressView_progressWorkoutValue, 0);
			strokeColor = typedArray.getInt(R.styleable.WorkoutProgressView_strokeColor, R.color.green);
		} finally {
			typedArray.recycle();
		}
	}

	public void setProgressRoundValue(int progressRoundValue) {
		this.progressRoundValue = progressRoundValue;
		this.invalidate();
	}

	public void setProgressWorkoutValue(int progressWorkoutValue) {
		this.progressWorkoutValue = progressWorkoutValue;
		this.invalidate();
	}

	public int getProgressRoundValue() {
		return progressRoundValue;
	}

	public int getProgressWorkoutValue() {
		return progressWorkoutValue;
	}

	public void setTimeText(String timeText) {
		this.timeText = timeText;
		this.invalidate();
	}

	public String getTimeText() {
		return timeText;
	}

	public void setStrokeColor(int color) {
		strokeColor = color;
		initStrokeColors();
	}

	private void initPaints() {

		strokeWidth = getResources().getDimensionPixelSize(R.dimen.progress_stroke_width);

		timeTextPaint = new TextPaint();
		setTextPaintCommontProperties(timeTextPaint, timeTextSize);

		Typeface robotoLight = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
		timeTextPaint.setTypeface(robotoLight);

		pauseTextPaint = new TextPaint();
		setTextPaintCommontProperties(pauseTextPaint, pauseTextSize);
		Typeface robotoBold = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
		pauseTextPaint.setTypeface(robotoBold);

		Paint progressBackgroundPaint = new Paint();
		setPaintCommonProperties(progressBackgroundPaint, Color.GRAY);
		progressBackgroundPaint.setAlpha(30);

		circleFillPaint = new Paint();
		setPaintCommonProperties(circleFillPaint, Color.GRAY);
		circleFillPaint.setAlpha(20);

		circleBackgroundPaint = new Paint();
		circleBackgroundPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		circleBackgroundPaint.setStyle(Paint.Style.STROKE);
		circleBackgroundPaint.setStrokeCap(Paint.Cap.ROUND);
		circleBackgroundPaint.setStrokeWidth(strokeWidth);
		circleBackgroundPaint.setColor(getResources().getColor(R.color.grey90));

		initStrokePaint(Color.MAGENTA);
	}

	private void initStrokePaint(int color) {
		circleStrokePaint.setDither(true);
		circleStrokePaint.setAntiAlias(true);
		circleStrokePaint.setStyle(Paint.Style.STROKE);
		circleStrokePaint.setStrokeCap(Paint.Cap.ROUND);
		circleStrokePaint.setStrokeWidth(strokeWidth);
		circleStrokePaint.setStrokeJoin(Paint.Join.MITER);
		circleStrokePaint.setColor(color);
	}

	private void setTextPaintCommontProperties(TextPaint textPaint, float textSize) {
		textPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		textPaint.setTextAlign(Paint.Align.CENTER);
		textPaint.setLinearText(true);
		textPaint.setColor(getResources().getColor(R.color.black));
		textPaint.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
		textPaint.setTextSize(textSize);
	}

	private void setPaintCommonProperties(Paint paint, int color) {
		paint.setFlags(Paint.ANTI_ALIAS_FLAG);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(color);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int width = getDefaultSize(getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight() + (int) strokeWidth, widthMeasureSpec);
		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);

		timeTextSize = parentWidth / 3.75f;
		pauseTextSize = timeTextSize / 5f;
		setMeasuredDimension(width, width);
		progressRect.set(strokeWidth + 20f, strokeWidth + 20f, width -
				(strokeWidth + 20f), width - (strokeWidth + 20f));
		initPaints();
	}

	private void initStrokeColors() {
		circleStrokePaint.setColor(strokeColor);
	}

	public void togglePause() {
		isPaused = !isPaused;
		if (isPaused) {
			startTimeBlinking();
		} else {
			stopBlinking();
		}
		invalidate();
	}

	private void startTimeBlinking() {
		if (animator == null) {
			animator = ValueAnimator.ofInt(255, 0);
		}

		animator.setDuration(1000);
		animator.setRepeatCount(ValueAnimator.INFINITE);
		animator.setRepeatMode(ValueAnimator.REVERSE);
		animator.setInterpolator(new LinearInterpolator());
		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				int alpha = (int) animation.getAnimatedValue();
				timeTextPaint.setAlpha(alpha);
				WorkoutProgressView.this.invalidate();
			}
		});
		animator.start();
	}

	private void stopBlinking() {
		if (animator == null) {
			return;
		}
		animator.end();
		timeTextPaint.setAlpha(255);
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		initStrokeColors();

		canvas.drawArc(progressRect, 0f, FULL_CIRCLE, false, circleBackgroundPaint);

		float roundProgressAngle = getRoundProgressAngle();

		canvas.drawArc(progressRect, START_ANGLE + 1, roundProgressAngle,
				false, circleStrokePaint);
		canvas.drawArc(progressRect, START_ANGLE, -(FULL_CIRCLE - getWorkoutProgressAngle()),
				true, circleFillPaint);

		if (timeText != null) {
			timeTextPaint.getTextBounds(timeText, 0, timeText.length(), textTimeBounds);
			int timeX = (getWidth() / 2);
			int timeY = (getHeight() / 2) + (textTimeBounds.height() / 2);
			canvas.drawText(timeText, timeX, timeY, timeTextPaint);
		}

		String pauseText = isPaused ? TEXT_RESUME : TEXT_PAUSE;
		pauseTextPaint.getTextBounds(pauseText, 0, pauseText.length(), textPauseBounds);
		int pauseX = getWidth() / 2;
		int pauseY = (getHeight() / 4) * 3 + textPauseBounds.height() * 2;
		canvas.drawText(pauseText, pauseX, pauseY, pauseTextPaint);
	}

	private float getRoundProgressAngle() {
		float progressPercentage = (float) progressRoundValue / MAX_PROGRESS_VALUE;
		return FULL_CIRCLE * progressPercentage;
	}

	private float getWorkoutProgressAngle() {
		float progressPercentage = (float) progressWorkoutValue / MAX_PROGRESS_VALUE;
		return FULL_CIRCLE * progressPercentage;
	}
}
