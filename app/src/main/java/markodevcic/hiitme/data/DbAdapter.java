package markodevcic.hiitme.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import markodevcic.hiitme.workout.History;
import markodevcic.hiitme.workout.WorkoutConfig;
import rx.Observable;
import rx.Subscriber;

public final class DbAdapter {

	public static final String DATABASE_NAME = "HIITdb";
	private static final int DATABASE_VERSION = 2;

	private static final String TABLE_SETTINGS_NAME = "Settings";
	private static final String TABLE_SETTINGS_COLUMN_ID = "SettingsID";
	private static final String TABLE_SETTINGS_COLUMN_ROUNDS = "Rounds";
	private static final String TABLE_SETTINGS_COLUMN_WORK = "WorkSeconds";
	private static final String TABLE_SETTINGS_COLUMN_REST = "RestSeconds";
	private static final String TABLE_SETTINGS_COLUMN_WARMUP = "WarmupSeconds";
	private static final String TABLE_SETTINGS_COLUMN_COOLDOWN = "CooldownSecons";
	private static final String TABLE_SETTINGS_COLUMN_PROFILE = "ProfileName";
	private static final String TABLE_SETTINGS_COLUMN_LAST_REST_CHECK = "LastRestDisabled";

	private static final String TABLE_HISTORY_NAME = "History";
	private static final String TABLE_HISTORY_COLUMN_ID = "HistoryID";
	private static final String TABLE_HISTORY_COLUMN_DATE = "DateCompleted";

	private static final String TABLE_SETTINGS_HISTORY_NAME = "SettingsHistory";
	private static final String TABLE_SETTINGS_HISTORY_COLUMN_ID = "SettingsHistoryID";
	private static final String TABLE_SETTINGS_HISTORY_DATE_FROM = "DateFrom";
	private static final String TABLE_SETTINGS_HISTORY_DATE_TO = "DateTo";


	private static final String SQL_CREATE_TABLE_SETTINGS = "CREATE TABLE " + TABLE_SETTINGS_NAME + " ( " +
			TABLE_SETTINGS_COLUMN_ID + " Integer primary key autoincrement, " +
			TABLE_SETTINGS_COLUMN_PROFILE + " text not null);";

	private static final String SQL_CREATE_TABLE_HISTORY = "CREATE TABLE " + TABLE_HISTORY_NAME + " (" +
			TABLE_HISTORY_COLUMN_ID + " integer primary key autoincrement, " +
			TABLE_HISTORY_COLUMN_DATE + " integer not null, " +
			TABLE_SETTINGS_COLUMN_ID + " int not null, foreign key (" + TABLE_SETTINGS_COLUMN_ID + ") references "
			+ TABLE_SETTINGS_NAME + "(" + TABLE_SETTINGS_COLUMN_ID + ") on delete cascade);";


	private static final String SQL_CREATE_TABLE_SETTINGSHISTORY = "CREATE TABLE " + TABLE_SETTINGS_HISTORY_NAME + " ( " +
			TABLE_SETTINGS_HISTORY_COLUMN_ID + " Integer primary key autoincrement, " +
			TABLE_SETTINGS_COLUMN_ROUNDS + " integer not null, " +
			TABLE_SETTINGS_COLUMN_WORK + " integer not null, " +
			TABLE_SETTINGS_COLUMN_REST + " integer not null, " +
			TABLE_SETTINGS_COLUMN_WARMUP + " integer not null, " +
			TABLE_SETTINGS_COLUMN_COOLDOWN + " integer not null, " +
			TABLE_SETTINGS_COLUMN_LAST_REST_CHECK + " bit not null, " +
			TABLE_SETTINGS_HISTORY_DATE_FROM + " integer not null, " +
			TABLE_SETTINGS_HISTORY_DATE_TO + " integer null, " +
			TABLE_SETTINGS_COLUMN_ID + " int not null, foreign key (" + TABLE_SETTINGS_COLUMN_ID + ") references "
			+ TABLE_SETTINGS_NAME + "(" + TABLE_SETTINGS_COLUMN_ID + ") on delete cascade);";

	private final String[] historyAllColumns = {TABLE_HISTORY_COLUMN_ID, TABLE_HISTORY_COLUMN_DATE, TABLE_SETTINGS_COLUMN_ID};

	private final DbContext dbcontext;
	private SQLiteDatabase db;

	public DbAdapter(Context context) {
		dbcontext = new DbContext(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void open() {
		try {
			db = dbcontext.getWritableDatabase();
		} catch (SQLiteException e) {
			db = dbcontext.getReadableDatabase();
		}
	}

	public void close() {
		if (db != null)
			db.close();
	}

	public long insertNewConfig(WorkoutConfig workoutConfig) {
		if (db == null)
			return -1;
		ContentValues settingContentValues = getSettingContentValues(workoutConfig);

		long settingsID = db.insert(TABLE_SETTINGS_NAME, null, settingContentValues);
		if (settingsID < 0)
			return settingsID;
		ContentValues settingHistoryContentValues = getSettingHistoryContentValues(workoutConfig, settingsID);
		return db.insert(TABLE_SETTINGS_HISTORY_NAME, null, settingHistoryContentValues);
	}

	public long updateSetting(WorkoutConfig workoutConfig, int rounds, int workSec, int restSec, int warmupSec, int cooldownSec,
							  String name, boolean disableRest) {
		if (!workoutConfig.getName().equals(name)) {
			ContentValues settingValues = new ContentValues();
			settingValues.put(TABLE_SETTINGS_COLUMN_PROFILE, name);
			db.update(TABLE_SETTINGS_NAME, settingValues, TABLE_SETTINGS_COLUMN_ID + " = " + String.valueOf(workoutConfig.getId()), null);
		}

		long dateNow = new Date().getTime();
		ContentValues oldHistoryValues = new ContentValues();
		oldHistoryValues.put(TABLE_SETTINGS_HISTORY_DATE_TO, dateNow);

		db.update(TABLE_SETTINGS_HISTORY_NAME, oldHistoryValues, TABLE_SETTINGS_COLUMN_ID + " = " + String.valueOf(workoutConfig.getId()) + " AND "
				+ TABLE_SETTINGS_HISTORY_DATE_TO + " is null", null);

		ContentValues newHistoryValues = getSettingHistoryContentValues(rounds, workSec, restSec, warmupSec, cooldownSec, disableRest, dateNow, workoutConfig.getId());
		return db.insert(TABLE_SETTINGS_HISTORY_NAME, null, newHistoryValues);
	}

	public List<WorkoutConfig> getAllConfigs() {
		Cursor cursor = getCursorAllSettings();
		List<WorkoutConfig> result = createConfigs(cursor);
		cursor.close();
		return result;
	}

	public Observable<List<WorkoutConfig>> queryConfigs() {
		open();
		List<WorkoutConfig> configs = getAllConfigs();
		Observable<List<WorkoutConfig>> observable = Observable.just(configs);
		close();
		return observable;
	}

	private WorkoutConfig getConfigByIdAndDate(int id, long date, boolean includeHistories) {
		Cursor cursor = getCursorSettingByIDAndDate(id, date);
		WorkoutConfig config = getConfig(cursor, includeHistories);
		cursor.close();
		return config;
	}

	public boolean deleteConfig(WorkoutConfig workoutConfig) {
		return db.delete(TABLE_SETTINGS_NAME, TABLE_SETTINGS_COLUMN_ID + " = " + workoutConfig.getId(), null) > 0;
	}

	private Cursor getCursorSettingByIDAndDate(int id, long date) {
		return db.rawQuery("SELECT * FROM " + TABLE_SETTINGS_NAME + " s " + "inner join " + TABLE_SETTINGS_HISTORY_NAME + " h " +
				" on s." + TABLE_SETTINGS_COLUMN_ID + " = h." + TABLE_SETTINGS_COLUMN_ID + " where s." + TABLE_SETTINGS_COLUMN_ID + " = ? AND ((h." +
				TABLE_SETTINGS_HISTORY_DATE_FROM + " <= ? AND h." + TABLE_SETTINGS_HISTORY_DATE_TO + " > ?) OR (h." + TABLE_SETTINGS_HISTORY_DATE_FROM + " <= ? AND h." +
				TABLE_SETTINGS_HISTORY_DATE_TO + " is null))", new String[]{String.valueOf(id), String.valueOf(date), String.valueOf(date), String.valueOf(date)});
	}

	private ContentValues getSettingContentValues(WorkoutConfig workoutConfig) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(TABLE_SETTINGS_COLUMN_PROFILE, workoutConfig.getName());
		return contentValues;
	}

	private ContentValues getSettingHistoryContentValues(WorkoutConfig workoutConfig, long settingsID) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(TABLE_SETTINGS_COLUMN_ROUNDS, workoutConfig.getRounds());
		contentValues.put(TABLE_SETTINGS_COLUMN_WORK, workoutConfig.getWorkSeconds());
		contentValues.put(TABLE_SETTINGS_COLUMN_REST, workoutConfig.getRestSeconds());
		contentValues.put(TABLE_SETTINGS_COLUMN_WARMUP, workoutConfig.getWarmupSeconds());
		contentValues.put(TABLE_SETTINGS_COLUMN_COOLDOWN, workoutConfig.getCooldownSeconds());
		contentValues.put(TABLE_SETTINGS_COLUMN_LAST_REST_CHECK, workoutConfig.isDisableLastRest() ? 1 : 0);
		contentValues.put(TABLE_SETTINGS_HISTORY_DATE_FROM, new Date().getTime());
		contentValues.put(TABLE_SETTINGS_COLUMN_ID, settingsID);
		return contentValues;
	}

	private ContentValues getSettingHistoryContentValues(int rounds, int work, int rest, int warmup, int cooldown, boolean disableRest, long date, int settingsID) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(TABLE_SETTINGS_COLUMN_ROUNDS, rounds);
		contentValues.put(TABLE_SETTINGS_COLUMN_WORK, work);
		contentValues.put(TABLE_SETTINGS_COLUMN_REST, rest);
		contentValues.put(TABLE_SETTINGS_COLUMN_WARMUP, warmup);
		contentValues.put(TABLE_SETTINGS_COLUMN_COOLDOWN, cooldown);
		contentValues.put(TABLE_SETTINGS_COLUMN_LAST_REST_CHECK, disableRest ? 1 : 0);
		contentValues.put(TABLE_SETTINGS_HISTORY_DATE_FROM, date);
		contentValues.put(TABLE_SETTINGS_COLUMN_ID, settingsID);
		return contentValues;
	}

	private Cursor getCursorAllSettings() {
		return db.rawQuery("SELECT * FROM " + TABLE_SETTINGS_NAME + " s " + "inner join " + TABLE_SETTINGS_HISTORY_NAME + " h " +
				" on s." + TABLE_SETTINGS_COLUMN_ID + " = h." + TABLE_SETTINGS_COLUMN_ID + " where h." +
				TABLE_SETTINGS_HISTORY_DATE_TO + " is null", new String[]{});
	}

	private WorkoutConfig getConfig(Cursor cursor, boolean includeHistories) {
		if (cursor == null)
			throw new IllegalArgumentException("cursor is null");
		if (cursor.moveToFirst()) {
			return createConfigFromCursor(cursor, includeHistories);
		} else {
			throw new IllegalStateException("cursor is empty");
		}
	}

	private WorkoutConfig createConfigFromCursor(Cursor cursor, boolean includeHistories) {
		int id = cursor.getInt(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_ID));
		int rounds = cursor.getInt(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_ROUNDS));
		int workSeconds = cursor.getInt(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_WORK));
		int restSeconds = cursor.getInt(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_REST));
		int warmupSeconds = cursor.getInt(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_WARMUP));
		int cooldownSeconds = cursor.getInt(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_COOLDOWN));
		String name = cursor.getString(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_PROFILE));
		boolean disableLastRest = cursor.getInt(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_LAST_REST_CHECK)) == 1;

		WorkoutConfig.Builder builder = new WorkoutConfig.Builder().id(id).rounds(rounds)
				.workSeconds(workSeconds).restSeconds(restSeconds).warmupSeconds(warmupSeconds)
				.cooldownSeconds(cooldownSeconds).name(name)
				.disableLastRest(disableLastRest);

		if (includeHistories) {
			List<History> histories = getHistories(id);
			builder.histories(histories);
		}

		return builder.build();
	}

	private List<WorkoutConfig> createConfigs(Cursor cursor) {
		if (cursor == null)
			throw new IllegalArgumentException("cursor is null");

		List<WorkoutConfig> workoutConfigs = new ArrayList<>();

		if (cursor.moveToFirst()) {
			do {
				WorkoutConfig workoutConfig = createConfigFromCursor(cursor, true);
				workoutConfigs.add(workoutConfig);
			} while (cursor.moveToNext());
		}

		return workoutConfigs;
	}

	public long insertHistory(History history) {
		if (db == null)
			return -1;

		ContentValues contentValues = new ContentValues();
		contentValues.put(TABLE_HISTORY_COLUMN_DATE, history.getDateCompleted());
		contentValues.put(TABLE_SETTINGS_COLUMN_ID, history.getWorkoutConfig().getId());
		return db.insert(TABLE_HISTORY_NAME, null, contentValues);
	}

	public List<History> getAllHistories() {
		Cursor cursor = getCursorAllHistories();
		List<History> result = cursorToAllHistories(cursor);
		cursor.close();
		return result;
	}

	public Observable<History> startHistoriesQuery() {
		return Observable.create(new Observable.OnSubscribe<History>() {
			@Override
			public void call(Subscriber<? super History> subscriber) {
				open();
				Cursor cursor = getCursorAllHistories();
				try {
					if (cursor.moveToFirst() && !subscriber.isUnsubscribed()) {
						do {
							History history = getHistoryFromCursor(cursor);
							subscriber.onNext(history);
						} while (cursor.moveToNext() && !subscriber.isUnsubscribed());
					}
					subscriber.onCompleted();
				} catch (Exception e) {
					subscriber.onError(e);
				} finally {
					cursor.close();
					close();
				}
			}
		});
	}

	public Observable<List<History>> queryHistories(){
		open();
		Observable<List<History>> observable = Observable.just(getAllHistories());
		close();
		return observable;
	}

	private List<History> getHistories(int settingID) {
		Cursor cursor = getCursorHistoriesBySettingID(settingID);
		List<History> result = cursorToAllHistoriesSafe(cursor);
		cursor.close();
		return result;
	}

	private Cursor getCursorAllHistories() {
		return db.query(TABLE_HISTORY_NAME, historyAllColumns, null, null, null, null, null);
	}

	private Cursor getCursorHistoriesBySettingID(int settingID) {
		return db.query(TABLE_HISTORY_NAME, historyAllColumns, TABLE_SETTINGS_COLUMN_ID + " = " + settingID, null, null, null, null);
	}

	private List<History> cursorToAllHistories(Cursor cursor) {
		if (cursor == null)
			return null;
		List<History> histories = new ArrayList<>();
		if (cursor.moveToFirst()) {
			do {
				History history = getHistoryFromCursor(cursor);
				histories.add(history);
			} while (cursor.moveToNext());
		}
		return histories;
	}

	private List<History> cursorToAllHistoriesSafe(Cursor cursor) {
		if (cursor == null)
			return null;
		List<History> histories = new ArrayList<>();
		if (cursor.moveToFirst()) {
			do {
				History history = getHistorySafeFromCursor(cursor);
				histories.add(history);
			} while (cursor.moveToNext());
		}
		return histories;
	}

	private History getHistoryFromCursor(Cursor cursor) {
		int ID = cursor.getInt(cursor.getColumnIndex(TABLE_HISTORY_COLUMN_ID));
		long date = cursor.getLong(cursor.getColumnIndex(TABLE_HISTORY_COLUMN_DATE));
		int settingsID = cursor.getInt(cursor.getColumnIndex(TABLE_SETTINGS_COLUMN_ID));
		WorkoutConfig workoutConfig = this.getConfigByIdAndDate(settingsID, date, true);
		return new History(ID, date, workoutConfig);
	}

	private History getHistorySafeFromCursor(Cursor cursor) {
		int ID = cursor.getInt(cursor.getColumnIndex(TABLE_HISTORY_COLUMN_ID));
		long date = cursor.getLong(cursor.getColumnIndex(TABLE_HISTORY_COLUMN_DATE));
		return new History(ID, date, null);
	}

	private final static class DbContext extends SQLiteOpenHelper {

		public DbContext(Context context, String name,
						 SQLiteDatabase.CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onOpen(SQLiteDatabase db) {

			super.onOpen(db);
			if (!db.isReadOnly()) {
				db.execSQL("PRAGMA foreign_keys=ON");
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if (newVersion == 2) {
				db.execSQL(SQL_CREATE_TABLE_SETTINGSHISTORY);
				db.execSQL("Insert into SettingsHistory(Rounds, WorkSeconds, RestSeconds, WarmupSeconds, CooldownSecons, LastRestDisabled, DateFrom, DateTo, SettingsID) " +
						"Select  Rounds, WorkSeconds, RestSeconds, WarmupSeconds, CooldownSecons, LastRestDisabled, " +
						"COALESCE((select  h.DateCompleted from Settings s1 left join History h on s1.SettingsID = h.SettingsID where s1.SettingsID = s.SettingsID order by h.DateCompleted asc limit 1), (strftime('%s', 'now')*1000)) DateFrom " +
						", null, s.SettingsID " +
						"from Settings  s");
				db.execSQL("CREATE TABLE SettingsTemp ( SettingsID Integer primary key autoincrement, ProfileName text not null)");
				db.execSQL("Insert into SettingsTemp(SettingsID, ProfileName) " +
						"Select SettingsID, ProfileName from Settings");
				db.execSQL("Drop table Settings");
				db.execSQL("Alter table SettingsTemp rename to Settings");
			}
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(SQL_CREATE_TABLE_SETTINGS);
			db.execSQL(SQL_CREATE_TABLE_HISTORY);
			db.execSQL(SQL_CREATE_TABLE_SETTINGSHISTORY);
		}
	}
}