package markodevcic.hiitme.activity;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import markodevcic.hiitme.HIITmeApplication;
import markodevcic.hiitme.R;
import markodevcic.hiitme.databinding.ActivityWorkoutBinding;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.WorkoutCompletedEvent;
import markodevcic.hiitme.fragments.MessageDialogFragment;
import markodevcic.hiitme.viewModels.WorkoutViewModel;
import markodevcic.hiitme.workout.Workout;
import markodevcic.hiitme.workout.WorkoutConfig;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

import static markodevcic.hiitme.utils.PreferenceHelper.PREF_KEEP_ON_TOP;

public class WorkoutActivity
		extends AppCompatActivity
		implements MessageDialogFragment.DialogClickListener {

	public static final String EXTRA_WORKOUT_CONFIG = "extra config";

	private static final String DIALOG_CANCEL_WORKOUT_TAG = "cancel workout";
	private static final int NOTIFICATION_ID = 101;

	private final EventBus eventBus = EventBus.getInstance();
	private final CompositeSubscription eventsSubscription = new CompositeSubscription();
	private boolean pauseOnInterrupt;
	private PowerManager.WakeLock wakeLock;
	private WorkoutViewModel viewModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		ActivityWorkoutBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_workout);
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WorkoutWakeLock");

		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		pauseOnInterrupt = sharedPreferences.getBoolean(PREF_KEEP_ON_TOP, true);

		initAndAssignTypeFace();

		Bundle bundle = getIntent().getExtras();
		if (bundle == null) {
			throw new IllegalStateException("Workout config expected in bundle");
		}
		WorkoutConfig workoutConfig = bundle.getParcelable(EXTRA_WORKOUT_CONFIG);
		viewModel = new WorkoutViewModel(this, workoutConfig);
		binding.setViewModel(viewModel);
		eventsSubscription.add(eventBus.subscribe(WorkoutCompletedEvent.class, new Action1<WorkoutCompletedEvent>() {
			@Override
			public void call(WorkoutCompletedEvent workoutCompletedEvent) {
				workoutCompleted();
			}
		}));
	}

	private void initAndAssignTypeFace() {
		TextView textViewCurrentRound = (TextView) findViewById(R.id.workout_textview_current_round);
		TextView textViewLastRound = (TextView) findViewById(R.id.workout_textview_last_round);
		TextView textViewRoundDivider = (TextView) findViewById(R.id.workout_textview_round_divider);
		TextView textViewRemainingTime = (TextView) findViewById(R.id.workout_textview_remaining_time);
		TextView textViewStatus = (TextView) findViewById(R.id.workout_textview_status);

		Typeface robotoLight = ((HIITmeApplication) getApplication()).getRobotoLightTypeFace();

		textViewStatus.setTypeface(robotoLight);
		textViewCurrentRound.setTypeface(robotoLight);
		textViewLastRound.setTypeface(robotoLight);
		textViewRemainingTime.setTypeface(robotoLight);
		textViewRoundDivider.setTypeface(robotoLight);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (pauseOnInterrupt && viewModel.getWorkoutStatus() != Workout.Status.COMPLETED) {
		/*
			checking for top activity immediately after pressing home button returns this as a top activity on some phones
            so the check is performed with a delay of 150ms
         */
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					Context context = getApplicationContext();
					ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
					List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
					if (!taskInfo.isEmpty()) {
						ComponentName topActivity = taskInfo.get(0).topActivity;
						if (!topActivity.getPackageName().equals(context.getPackageName())) {
							WorkoutActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									wakeLock.release();
									if (viewModel.getWorkoutStatus() == Workout.Status.RUNNING) {
										viewModel.stop();
										setNotification();
										Toast.makeText(WorkoutActivity.this, "Workout paused!", Toast.LENGTH_SHORT).show();
									}
								}
							});
						}
					}
				}
			}, 150);
		}
	}

	private void setNotification() {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		builder.setSmallIcon(R.drawable.ic_stat_stoperica)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_app_launcher))
				.setContentTitle("HIITme")
				.setContentText("Workout paused.");

		Intent intent = new Intent(this, this.getClass());
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		PendingIntent pendingIntent = PendingIntent.getActivity(
				this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

		builder.setContentIntent(pendingIntent);

		NotificationManager notificationManager = (NotificationManager) getSystemService(
				Context.NOTIFICATION_SERVICE);
		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!wakeLock.isHeld()) {
			wakeLock.acquire();
		}
		if (pauseOnInterrupt) {
			clearNotification();
		}
		if (viewModel.getWorkoutStatus() == Workout.Status.STOPPED && !viewModel.isWorkoutPaused()) {
			viewModel.start();
		}
	}

	private void clearNotification() {
		NotificationManager notificationManager = (NotificationManager) getSystemService(
				Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(NOTIFICATION_ID);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (wakeLock.isHeld()) {
			wakeLock.release();
		}

		viewModel.destroy();
		eventsSubscription.unsubscribe();
	}

	@Override
	public void onBackPressed() {
		if (viewModel.getWorkoutStatus() != Workout.Status.COMPLETED) {
			MessageDialogFragment messageDialogFragment = new MessageDialogFragment();
			Bundle bundle = new Bundle();
			bundle.putBoolean("okOnly", false);
			bundle.putString("message", "Cancel workout?");
			bundle.putString("title", "Confirm ... ");
			bundle.putString("positiveButton", "Yes");
			bundle.putString("negativeButton", "No");
			messageDialogFragment.setArguments(bundle);
			messageDialogFragment.show(getSupportFragmentManager(), DIALOG_CANCEL_WORKOUT_TAG);
		} else {
			workoutCompleted();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	public void onPositiveClick(DialogFragment dialogFragment) {
		if (dialogFragment.getTag().equals(DIALOG_CANCEL_WORKOUT_TAG)) {
			viewModel.stop();
			setResult(RESULT_CANCELED);
			this.finish();
		} else {
			this.finish();
		}
	}

	@Override
	public void onNegativeClick(DialogFragment dialogFragment) {
		//do nothing, workout continues
	}

	private void workoutCompleted() {
		setResult(RESULT_OK);
		finish();
	}
}