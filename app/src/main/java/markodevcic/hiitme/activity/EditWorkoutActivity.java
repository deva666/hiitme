package markodevcic.hiitme.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import markodevcic.hiitme.R;
import markodevcic.hiitme.databinding.ActivityEditWorkoutBinding;
import markodevcic.hiitme.viewModels.EditWorkoutViewModel;
import markodevcic.hiitme.workout.WorkoutConfig;
import rx.functions.Action1;

public class EditWorkoutActivity extends AppCompatActivity {

	public static final int RESULT_WORKOUT_ADDED = 111;

	private EditWorkoutViewModel editWorkoutViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		ActivityEditWorkoutBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_workout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

		Bundle bundle = getIntent().getExtras();
		WorkoutConfig workoutConfig;
		if (bundle == null) {
			workoutConfig = null;
			actionBar.setTitle("New workout");
		} else {
            workoutConfig = bundle.getParcelable(WorkoutActivity.EXTRA_WORKOUT_CONFIG);
			actionBar.setTitle("Edit workout");
        }
		editWorkoutViewModel = new EditWorkoutViewModel(this,
				this.getSupportFragmentManager(), findViewById(R.id.profile_coordinator), workoutConfig);
		editWorkoutViewModel.getWorkoutSavedObservable().subscribe(new Action1<Boolean>() {
			@Override
			public void call(Boolean aBoolean) {
				setResult(RESULT_WORKOUT_ADDED);
				finish();
			}
		});
		binding.setViewModel(editWorkoutViewModel);
    }
}
