package markodevcic.hiitme.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import markodevcic.hiitme.R;
import markodevcic.hiitme.backup.GoogleDriveService;
import markodevcic.hiitme.backup.RestoreDbCallback;
import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.events.BackupDbEvent;
import markodevcic.hiitme.events.ConfigCreatedEvent;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.GoogleDriveConnectionEvent;
import markodevcic.hiitme.events.HistoryChangedEvent;
import markodevcic.hiitme.fragments.AboutFragment;
import markodevcic.hiitme.fragments.MessageDialogFragment;
import markodevcic.hiitme.fragments.SettingsFragment;
import markodevcic.hiitme.fragments.StatisticsFragment;
import markodevcic.hiitme.fragments.WorkoutsFragment;
import markodevcic.hiitme.utils.PreferenceHelper;
import markodevcic.hiitme.utils.TimeFormatter;
import markodevcic.hiitme.workout.History;
import markodevcic.hiitme.workout.WorkoutConfig;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;


public class HomeActivity extends AppCompatActivity
		implements MessageDialogFragment.DialogClickListener,
		FragmentManager.OnBackStackChangedListener,
		Observer<List<History>> {

	public static final int NEW_PROFILE_ACTIVITY_RESULT = 1;
	public static final int NEW_WORKOUT_ACTIVITY_RESULT = 2;

	private static final String STATISTICS_FRAGMENT_TAG = "statistics_fragment";
	private static final String PROFILES_FRAGMENT_TAG = "profiles_fragment";
	private static final String SETTINGS_FRAGMENT_TAG = "settings_fragment";
	private static final String ABOUT_FRAGMENT_TAG = "about_fragment";
	private static final String SELECTED_CHILD_POSITION_KEY = "selected_child_position";

	private final List<WeakReference<Fragment>> fragmentsList = new ArrayList<>();
	private final EventBus eventBus = EventBus.getInstance();
	private final CompositeSubscription eventSubscriptions = new CompositeSubscription();
	private Subscription historiesSubscription = Subscriptions.unsubscribed();

	private CharSequence title;
	private DbAdapter dbAdapter;

	private DrawerLayout drawerLayout;
	private TextView textViewWorkouts;
	private TextView textViewTotalTime;
	private TextView textViewLastWorkout;

	private Menu navMenu;

	private int selectedChildPosition;

	private GoogleDriveService driveService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
		actionBar.setDisplayHomeAsUpEnabled(true);

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		View headerView = navigationView.inflateHeaderView(R.layout.nav_header);
		setupDrawerContent(navigationView);
		navMenu = navigationView.getMenu();
		textViewWorkouts = (TextView) headerView.findViewById(R.id.nav_text_workouts);
		textViewTotalTime = (TextView) headerView.findViewById(R.id.nav_text_total_time);
		textViewLastWorkout = (TextView) headerView.findViewById(R.id.nav_text_last_workout);

		if (savedInstanceState == null) {
			PreferenceManager.setDefaultValues(this, R.xml.prefrences, false);
			navigateToWorkouts();
			navMenu.findItem(R.id.nav_workouts).setChecked(true);
			title = getString(R.string.workouts_section);
			setTitleBasedOnChild();
		} else {
			selectedChildPosition = savedInstanceState.getInt(SELECTED_CHILD_POSITION_KEY);
			setTitleBasedOnPosition();
		}

		getSupportFragmentManager().addOnBackStackChangedListener(this);

		driveService = new GoogleDriveService(this);
		eventSubscriptions.add(eventBus.subscribe(ConfigCreatedEvent.class, new Action1<ConfigCreatedEvent>() {
			@Override
			public void call(ConfigCreatedEvent configCreatedEvent) {
				Snackbar.make(findViewById(R.id.home_coordinator), "Workout created!", Snackbar
						.LENGTH_LONG).show();
			}
		}));
		eventSubscriptions.add(eventBus.subscribe(HistoryChangedEvent.class, new Action1<HistoryChangedEvent>() {
			@Override
			public void call(HistoryChangedEvent historyChangedEvent) {
				queryHistories();
			}
		}));
		eventSubscriptions.add(eventBus.subscribe(BackupDbEvent.class, new Action1<BackupDbEvent>() {
			@Override
			public void call(BackupDbEvent backupDbEvent) {
				SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
				if (sharedPreferences.getBoolean(PreferenceHelper.PREF_BACKUP_DB, false)) {
					driveService.backup();
				}
			}
		}));

		dbAdapter = new DbAdapter(this);
		queryHistories();

		if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(PreferenceHelper.PREF_BACKUP_DB, false)) {
			driveService.connect();
		}
	}

	private void setupDrawerContent(NavigationView navigationView) {
		navigationView.setNavigationItemSelectedListener(
				new NavigationView.OnNavigationItemSelectedListener() {
					@Override
					public boolean onNavigationItemSelected(MenuItem menuItem) {
						menuItem.setChecked(true);
						drawerLayout.closeDrawers();
						navigateToChild(menuItem);
						return true;
					}
				});
	}

	private void navigateToChild(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case R.id.nav_history:
				navigateToStatistics();
				break;
			case R.id.nav_workouts:
				navigateToWorkouts();
				break;
			case R.id.nav_settings:
				navigateToSettings();
				break;
			case R.id.nav_about:
				navigateToAbout();
				break;
		}
	}

	private void queryHistories() {
		historiesSubscription.unsubscribe();
		historiesSubscription = dbAdapter.queryHistories()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this);
	}

	public void restoreBackup(RestoreDbCallback callback) {
		driveService.restoreBackup(callback);
	}

	public void connectToGoogleDrive() {
		driveService.connect();
	}

	public void connectToGoogleDriveAndBackup() {
		driveService.connectAndBackup();
	}

	public void disconnectGoogleDrive() {
		driveService.disconnect();
	}

	public boolean isGoogleDriveConnected() {
		return driveService.isConnected();
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		fragmentsList.add(new WeakReference<>(fragment));
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		getSupportFragmentManager().removeOnBackStackChangedListener(this);
		eventSubscriptions.unsubscribe();
		driveService.disconnect();
	}

	private void navigateToStatistics() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment statisticsFragment = fragmentManager.findFragmentByTag(STATISTICS_FRAGMENT_TAG);

		if (statisticsFragment == null) {
			statisticsFragment = new StatisticsFragment();
		}
		startFragmentTransaction(fragmentManager, statisticsFragment, STATISTICS_FRAGMENT_TAG);
	}

	private void navigateToWorkouts() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment workoutsFragment = fragmentManager.findFragmentByTag(PROFILES_FRAGMENT_TAG);

		if (workoutsFragment == null) {
			workoutsFragment = new WorkoutsFragment();
		}

		if (fragmentManager.getBackStackEntryCount() > 0) {
			fragmentManager.beginTransaction()
					.setCustomAnimations(R.anim.fade_out, R.anim.fade_in,
							R.anim.fade_out, R.anim.fade_in)
					.replace(R.id.container, workoutsFragment, PROFILES_FRAGMENT_TAG)
					.addToBackStack(null)
					.commit();
		} else {
			fragmentManager.beginTransaction()
					.setCustomAnimations(R.anim.fade_out, R.anim.fade_in,
							R.anim.fade_out, R.anim.fade_in)
					.replace(R.id.container, workoutsFragment, PROFILES_FRAGMENT_TAG)
					.commit();
		}
	}

	private void navigateToSettings() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment settingsFragment = fragmentManager.findFragmentByTag(SETTINGS_FRAGMENT_TAG);

		if (settingsFragment == null) {
			settingsFragment = new SettingsFragment();
		}
		startFragmentTransaction(fragmentManager, settingsFragment, SETTINGS_FRAGMENT_TAG);
	}

	private void navigateToAbout() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment aboutFragment = fragmentManager.findFragmentByTag(ABOUT_FRAGMENT_TAG);

		if (aboutFragment == null) {
			aboutFragment = new AboutFragment();
		}
		startFragmentTransaction(fragmentManager, aboutFragment, ABOUT_FRAGMENT_TAG);
	}

	private static void startFragmentTransaction(FragmentManager fragmentManager, Fragment fragment, String tag) {
		fragmentManager.beginTransaction()
				.setCustomAnimations(R.anim.fade_out, R.anim.fade_in,
						R.anim.fade_out, R.anim.fade_in)
				.replace(R.id.container, fragment, tag)
				.addToBackStack(null)
				.commit();
	}

	private Iterable<Fragment> getActiveFragments() {
		Collection<Fragment> returnList = new ArrayList<>();
		int size = fragmentsList.size();
		for (int i = 0; i < size; i++) {
			Fragment fragment = fragmentsList.get(i).get();
			if (fragment != null && fragment.isVisible()) {
				returnList.add(fragment);
			}
		}
		return returnList;
	}

	private void restoreActionBar() {
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setTitle(title);
	}

	private void setTitleBasedOnChild() {
		String tag = "";
		for (Fragment fragment : getActiveFragments()) {
			tag = fragment.getTag();
		}
		if (tag == null) {
			return;
		}
		switch (tag) {
			case STATISTICS_FRAGMENT_TAG:
				selectedChildPosition = 1;
				title = getResources().getString(R.string.history_section);
				navMenu.findItem(R.id.nav_history).setChecked(true);
				break;
			case PROFILES_FRAGMENT_TAG:
				selectedChildPosition = 2;
				title = getResources().getString(R.string.workouts_section);
				navMenu.findItem(R.id.nav_workouts).setChecked(true);
				break;
			case SETTINGS_FRAGMENT_TAG:
				selectedChildPosition = 3;
				title = getResources().getString(R.string.settings_section);
				navMenu.findItem(R.id.nav_settings).setChecked(true);
				break;
		}
		restoreActionBar();
	}

	private void setTitleBasedOnPosition() {
		switch (selectedChildPosition) {
			case 0:
				title = getResources().getString(R.string.workouts_section);
				break;
			case 1:
				title = getResources().getString(R.string.history_section);
				break;
			case 2:
				title = getResources().getString(R.string.workouts_section);
				break;
			case 3:
				title = getResources().getString(R.string.settings_section);
				break;
		}
		restoreActionBar();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == GoogleDriveService.GOOGLE_DRIVE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				driveService.connect();
			} else if (resultCode == RESULT_CANCELED) {
				EventBus.getInstance().post(new GoogleDriveConnectionEvent(false));
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_music) {
			startMusicPlayer();
			return true;
		} else if (id == android.R.id.home) {
			drawerLayout.openDrawer(GravityCompat.START);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void startMusicPlayer() {
		try {
			if (android.os.Build.VERSION.SDK_INT >= 15) {
				Intent intent = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN,
						Intent.CATEGORY_APP_MUSIC);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//Min SDK 15
				startActivity(intent);
			} else {
				Intent intent = new Intent(MediaStore.INTENT_ACTION_MUSIC_PLAYER);//Min SDK 8
				startActivity(intent);
			}
		} catch (Exception e) {
			Toast.makeText(this, "Music player not found", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onPositiveClick(android.support.v4.app.DialogFragment dialogFragment) {
		//do nothing
	}

	@Override
	public void onNegativeClick(android.support.v4.app.DialogFragment dialogFragment) {
		//do nothing
	}

	@Override
	public void onBackStackChanged() {
		setTitleBasedOnChild();
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {

	}

	@Override
	public void onNext(List<History> histories) {
		fillInfoView(histories);
	}

	private void fillInfoView(List<History> histories) {
		textViewWorkouts.setText(String.valueOf(histories.size()));
		if (!histories.isEmpty()) {
			Collections.sort(histories);
			History lastHistory = histories.get(histories.size() - 1);
			DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault());
			String dateFormatted = dateFormat.format(new Date(lastHistory.getDateCompleted()));
			textViewLastWorkout.setText(dateFormatted);

			int totalTime = 0;
			int size = histories.size();
			for (int i = 0; i < size; i++) {
				WorkoutConfig workoutConfig = histories.get(i).getWorkoutConfig();
				totalTime += workoutConfig.getTotalTime();
			}
			textViewTotalTime.setText(TimeFormatter.format(totalTime));
		} else {
			textViewLastWorkout.setText("-");
			textViewTotalTime.setText("00:00");
		}
	}
}