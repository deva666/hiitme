package markodevcic.hiitme.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import markodevcic.hiitme.R;
import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.fragments.CreateWorkoutFragment;
import markodevcic.hiitme.fragments.FirstPageFragment;
import markodevcic.hiitme.workout.WorkoutConfig;

public class StartupActivity extends AppCompatActivity {

	private ViewPager viewPager;
	private FirstPageFragment firstPageFragment;
	private CreateWorkoutFragment createWorkoutFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!tryStartHomeActivity()) {
			initViews();
		}
	}

	private boolean tryStartHomeActivity() {
		DbAdapter dbAdapter = new DbAdapter(this);
		dbAdapter.open();
		List<WorkoutConfig> workoutConfigs = dbAdapter.getAllConfigs();
		dbAdapter.close();
		if (!workoutConfigs.isEmpty()) {
			startHomeActivity();
			return true;
		}
		return false;
	}

	private void startHomeActivity() {
		Intent intent = new Intent(this, HomeActivity.class);
		startActivity(intent);
	}

	private void initViews() {
		setContentView(R.layout.startup_pager);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		viewPager = (ViewPager) findViewById(R.id.pager);
		PagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(pagerAdapter);
	}

	@Override
	public void onBackPressed() {
		if (viewPager.getCurrentItem() == 0) {
			super.onBackPressed();
		} else {
			viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
		}
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			if (position == 0) {
				return firstPageFragment == null ? firstPageFragment = new FirstPageFragment() : firstPageFragment;
			} else {
				return createWorkoutFragment == null ? createWorkoutFragment = new CreateWorkoutFragment() :
						createWorkoutFragment;
			}
		}

		@Override
		public int getCount() {
			return 2;
		}
	}
}
