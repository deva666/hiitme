package markodevcic.hiitme.workout;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public final class WorkoutConfig
		implements Parcelable, Comparable<WorkoutConfig>{

	private final int id;
	private final int rounds;
	private final int workSeconds;
	private final int restSeconds;
	private final int warmupSeconds;
	private final int cooldownSeconds;
	private final String name;
	private final List<History> histories;
	private final boolean disableLastRest;

	private WorkoutConfig(Builder builder) {
		this.id = builder.id;
		this.rounds = builder.rounds;
		this.workSeconds = builder.workSeconds;
		this.restSeconds = builder.restSeconds;
		this.name = builder.name;
		this.warmupSeconds = builder.warmupSeconds;
		this.cooldownSeconds = builder.cooldownSeconds;
		this.histories = builder.histories;
		this.disableLastRest = builder.disableLastRest;
	}

	public int getId() {
		return id;
	}

	public int getRounds() {
		return rounds;
	}

	public int getWorkSeconds() {
		return workSeconds;
	}

	public int getRestSeconds() {
		return restSeconds;
	}

	public int getWarmupSeconds() {
		return warmupSeconds;
	}

	public int getCooldownSeconds() {
		return cooldownSeconds;
	}

	public String getName() {
		return name;
	}

	public List<History> getHistories() {
		return histories;
	}

	public boolean isDisableLastRest() {
		return disableLastRest;
	}

	public int getTotalTime() {
		int totalTime;
		totalTime = disableLastRest ? rounds * (workSeconds + restSeconds) - restSeconds + warmupSeconds +
				cooldownSeconds : rounds * (workSeconds + restSeconds) + warmupSeconds + cooldownSeconds;
		return totalTime;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeInt(rounds);
		dest.writeInt(workSeconds);
		dest.writeInt(restSeconds);
		dest.writeInt(warmupSeconds);
		dest.writeInt(cooldownSeconds);
		dest.writeString(name);
		dest.writeList(histories);
		dest.writeByte(disableLastRest ? (byte)1: (byte)0);
	}

	public static final Creator<WorkoutConfig> CREATOR = new Creator<WorkoutConfig>() {
		@SuppressWarnings("unchecked")
		@Override
		public WorkoutConfig createFromParcel(Parcel source) {
			WorkoutConfig.Builder builder = new Builder();
			builder.id = source.readInt();
			builder.rounds = source.readInt();
			builder.workSeconds = source.readInt();
			builder.restSeconds = source.readInt();
			builder.warmupSeconds = source.readInt();
			builder.cooldownSeconds = source.readInt();
			builder.name = source.readString();
			builder.histories = source.readArrayList(History.class.getClassLoader());
			builder.disableLastRest = source.readByte() != 0;
			return builder.build();
		}

		@Override
		public WorkoutConfig[] newArray(int size) {
			return new WorkoutConfig[size];
		}
	};

	@Override
	public int compareTo(WorkoutConfig another) {
		return Integer.valueOf(another.histories.size()).compareTo(histories.size());
	}

	public static class Builder {
		private int id;
		private int rounds;
		private int workSeconds;
		private int restSeconds;
		private int warmupSeconds;
		private int cooldownSeconds;
		private String name;
		private List<History> histories;
		private boolean disableLastRest;

		public WorkoutConfig build() {
			return new WorkoutConfig(this);
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder rounds(int rounds) {
			this.rounds = rounds;
			return this;
		}

		public Builder workSeconds(int workSeconds) {
			this.workSeconds = workSeconds;
			return this;
		}

		public Builder restSeconds(int restSeconds) {
			this.restSeconds = restSeconds;
			return this;
		}

		public Builder warmupSeconds(int warmupSeconds) {
			this.warmupSeconds = warmupSeconds;
			return this;
		}

		public Builder cooldownSeconds(int cooldownSeconds) {
			this.cooldownSeconds = cooldownSeconds;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder histories(List<History> histories) {
			this.histories = histories;
			return this;
		}

		public Builder disableLastRest(boolean disableLastRest) {
			this.disableLastRest = disableLastRest;
			return this;
		}
	}
}