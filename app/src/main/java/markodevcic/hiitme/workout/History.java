package markodevcic.hiitme.workout;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.LocalDate;

import java.util.Date;

public final class History
		implements Parcelable, Comparable<History> {

	private int historyID;
	private final long dateCompleted;
	private final WorkoutConfig workoutConfig;

	public History(int historyID, long dateCompleted, WorkoutConfig workoutConfig) {
		this.historyID = historyID;
		this.dateCompleted = dateCompleted;
		this.workoutConfig = workoutConfig;
	}

	public History(long dateCompleted, WorkoutConfig workoutConfig) {
		this.dateCompleted = dateCompleted;
		this.workoutConfig = workoutConfig;
	}

	History(Parcel in) {
		historyID = in.readInt();
		dateCompleted = in.readLong();
		workoutConfig = in.readParcelable(WorkoutConfig.class.getClassLoader());
	}

	public long getDateCompleted() {
		return dateCompleted;
	}

	public WorkoutConfig getWorkoutConfig() {
		return workoutConfig;
	}

	public Date getDate() {
		return new Date(dateCompleted);
	}

	public LocalDate getLocalDate() {
		return new LocalDate(dateCompleted);
	}

	public static final Creator<History> CREATOR = new Creator<History>() {
		@Override
		public History createFromParcel(Parcel in) {
			return new History(in);
		}

		@Override
		public History[] newArray(int size) {
			return new History[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeInt(historyID);
		dest.writeLong(dateCompleted);
		dest.writeParcelable(workoutConfig, flags);
	}

	@Override
	public int compareTo(History another) {
		return getDate().compareTo(another.getDate());
	}
}
