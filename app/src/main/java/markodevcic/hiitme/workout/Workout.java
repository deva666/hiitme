package markodevcic.hiitme.workout;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subjects.ReplaySubject;

public final class Workout {

	public static final String WORK_STATE = "WORK";
	public static final String REST_STATE = "REST";
	public static final String WARMUP_STATE = "WARMUP";
	public static final String COOLDOWN_STATE = "COOLDOWN";

	private static final long TIMER_TICK = 10;
	private static final int MAX_PROGRESS_VALUE = 10000;

	private final Observable<Long> timer = Observable.interval(TIMER_TICK, TimeUnit.MILLISECONDS, Schedulers.io());
	private final ReplaySubject<String> stateSubject = ReplaySubject.create();
	private final ReplaySubject<Status> statusSubject = ReplaySubject.create();
	private final ReplaySubject<Integer> workoutSecondsSubject = ReplaySubject.create();
	private final ReplaySubject<Integer> roundSecondsSubject = ReplaySubject.create();
	private final PublishSubject<Integer> roundProgressSubject = PublishSubject.create();
	private final PublishSubject<Integer> workoutProgressSubject = PublishSubject.create();
	private final ReplaySubject<Integer> currentRoundSubject = ReplaySubject.create();
	private final ReplaySubject<Integer> totalRoundsSubject = ReplaySubject.create();
	private final Executor workoutExecutor = Executors.newSingleThreadExecutor();

	private int currentMilliseconds;
	private int totalRounds;
	private int currentRound;
	private int workSeconds;
	private int restSeconds;
	private int cooldownSeconds;
	private int workoutSeconds;
	private int workoutTotalSeconds;
	private int currentRoundSeconds;
	private int currentRoundTotalSeconds;
	private boolean disableLastRest;
	private String currentStateName;
	private State currentWorkoutState;

	private Subscription timerSubscription;

	public Workout(final WorkoutConfig workoutConfig) {
		workoutExecutor.execute(new Runnable() {
			@Override
			public void run() {
				currentMilliseconds = 1000;
				disableLastRest = workoutConfig.isDisableLastRest();
				totalRounds = workoutConfig.getRounds();
				currentRound = 0;
				workSeconds = workoutConfig.getWorkSeconds();
				restSeconds = workoutConfig.getRestSeconds();
				cooldownSeconds = workoutConfig.getCooldownSeconds();
				workoutSeconds = workoutConfig.getTotalTime();
				workoutTotalSeconds = workoutSeconds;

				if (workoutConfig.getWarmupSeconds() > 0) {
					currentRoundSeconds = workoutConfig.getWarmupSeconds();
					currentRoundTotalSeconds = workoutConfig.getWarmupSeconds();
					currentWorkoutState = new WarmupState();
				} else {
					currentRoundSeconds = workSeconds;
					currentRoundTotalSeconds = workSeconds;
					currentRound = 1;
					currentWorkoutState = new WorkState();
				}
				currentStateName = currentWorkoutState.name();

				stateSubject.onNext(currentStateName);
				totalRoundsSubject.onNext(totalRounds);
				workoutSecondsSubject.onNext(workoutSeconds);
				roundSecondsSubject.onNext(currentRoundSeconds);
				currentRoundSubject.onNext(currentRound);
			}
		});
	}

	public Observable<Integer> getWorkoutSeconds() {
		return workoutSecondsSubject.asObservable();
	}

	public Observable<Integer> getRoundSeconds() {
		return roundSecondsSubject.asObservable();
	}

	public Observable<Integer> getRoundProgress() {
		return roundProgressSubject.asObservable();
	}

	public Observable<Integer> getWorkoutProgress() {
		return workoutProgressSubject.asObservable();
	}

	public Observable<String> getState() {
		return stateSubject.asObservable();
	}

	public Observable<Status> getStatus() {
		return statusSubject.asObservable();
	}

	public Observable<Integer> getCurrentRound() {
		return currentRoundSubject.asObservable();
	}

	public Observable<Integer> getTotalRounds() {
		return totalRoundsSubject.asObservable();
	}

	public synchronized void start() {
		subscribe();
		statusSubject.onNext(Status.RUNNING);
	}

	private synchronized void subscribe() {
		timerSubscription = timer
				.observeOn(Schedulers.from(workoutExecutor))
				.subscribe(new Action1<Long>() {
					@Override
					public void call(Long aLong) {
						onTimerTick();
					}
				});
	}

	public synchronized void stop() {
		unsubscribe();
		statusSubject.onNext(Status.STOPPED);
	}

	private synchronized void unsubscribe() {
		if (timerSubscription != null && !timerSubscription.isUnsubscribed()) {
			timerSubscription.unsubscribe();
		}
	}

	private void onTimerTick() {
		currentMilliseconds -= TIMER_TICK;

		if (currentMilliseconds == 0) {
			currentMilliseconds = 1000;

			workoutSeconds--;
			if (workoutSeconds == 0) {

				currentRoundSeconds = currentRoundTotalSeconds = 0;
				notifyTimeUpdated();
				notifyProgressUpdated();
				notifyCompleted();
				return;
			}

			currentRoundSeconds--;
			if (currentRoundSeconds == 0) {
				roundEnded();
				notifyProgressUpdated();
				notifyStateChanged();
			}
			notifyTimeUpdated();
		} else {
			notifyProgressUpdated();
		}
	}

	private void notifyCompleted() {
		unsubscribe();
		statusSubject.onNext(Status.COMPLETED);
	}

	private void notifyStateChanged() {
		stateSubject.onNext(currentStateName);
		currentRoundSubject.onNext(currentRound);
	}

	private void notifyTimeUpdated() {
		roundSecondsSubject.onNext(currentRoundSeconds);
		workoutSecondsSubject.onNext(workoutSeconds);
	}

	private void notifyProgressUpdated() {
		double roundSeconds = ((currentRoundSeconds - 1d) + (double) currentMilliseconds / 1000d);
		double roundProgress = (1d - (roundSeconds / currentRoundTotalSeconds)) * MAX_PROGRESS_VALUE;
		double workoutSecs = ((double) (workoutSeconds - 1) + (double) currentMilliseconds / 1000);
		double workoutProgress = ((workoutTotalSeconds - workoutSecs) / workoutTotalSeconds) * MAX_PROGRESS_VALUE;
		this.roundProgressSubject.onNext((int) roundProgress);
		this.workoutProgressSubject.onNext((int) workoutProgress);
	}

	private void roundEnded() {
		if (currentWorkoutState != null) {
			currentWorkoutState = currentWorkoutState.next();
		}
	}

	private abstract static class State {
		abstract State next();
		public abstract String name();
	}

	private class WorkState
			extends State {

		@Override
		State next() {
			State nextState;
			if (currentRound == totalRounds
					&& disableLastRest && cooldownSeconds > 0) {
				currentRoundSeconds = currentRoundTotalSeconds = cooldownSeconds;
				 nextState = new CooldownState();
			} else {
				currentRoundSeconds = currentRoundTotalSeconds = restSeconds;
				nextState = new RestState();
			}
			currentStateName = nextState.name();
			return nextState;
		}

		@Override
		public String name() {
			return WORK_STATE;
		}
	}

	private class RestState
			extends State {

		@Override
		State next() {
			State nextState;
			if (currentRound == totalRounds && cooldownSeconds > 0) {
				currentRoundSeconds = currentRoundTotalSeconds = cooldownSeconds;
				nextState = new CooldownState();
			} else {
				currentRoundSeconds = currentRoundTotalSeconds = workSeconds;
				currentRound++;
				nextState = new WorkState();
			}
			currentStateName = nextState.name();
			return nextState;
		}

		@Override
		public String name() {
			return REST_STATE;
		}
	}

	private static class CooldownState
			extends State {

		@Override
		State next() {
			return null;
		}

		@Override
		public String name() {
			return COOLDOWN_STATE;
		}
	}

	private class WarmupState
			extends State {

		@Override
		State next() {
			currentRound++;
			currentRoundSeconds = currentRoundTotalSeconds = workSeconds;
			State nextState = new WorkState();
			currentStateName = nextState.name();
			return nextState;
		}

		@Override
		public String name() {
			return WARMUP_STATE;
		}
	}

	public enum Status {
		RUNNING, STOPPED, COMPLETED
	}
}