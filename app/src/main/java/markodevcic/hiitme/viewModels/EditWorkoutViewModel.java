package markodevcic.hiitme.viewModels;

import android.content.Context;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import java.util.List;

import markodevcic.hiitme.R;
import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.events.ConfigCreatedEvent;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.fragments.IntervalDialogFragment;
import markodevcic.hiitme.utils.TimeFormatter;
import markodevcic.hiitme.workout.WorkoutConfig;
import rx.subjects.PublishSubject;

@SuppressWarnings("unused")
public final class EditWorkoutViewModel implements IntervalDialogFragment.IntervalDialogListener {

	public final ObservableField<String> roundsText = new ObservableField<>();
	public final ObservableField<String> warmupTime = new ObservableField<>();
	public final ObservableField<String> workTime = new ObservableField<>();
	public final ObservableField<String> restTime = new ObservableField<>();
	public final ObservableField<String> cooldownTime = new ObservableField<>();
	public final ObservableField<String> totalTime = new ObservableField<>();
	public final ObservableField<String> workoutName = new ObservableField<>("");
	public final ObservableBoolean disableLastRest = new ObservableBoolean();

	private final Context context;
	private final FragmentManager fragmentManager;
	private final View hostView;
	private final WorkoutConfig workoutConfig;
	private final PublishSubject<Boolean> workoutSaved = PublishSubject.create();
	private final boolean isNewConfig;

	private int rounds = 10;
	private int workSeconds = 60;
	private int restSeconds = 45;
	private int warmupSeconds = 3 * 60;
	private int cooldownSeconds = 3 * 60;
	private int totalSeconds;

	public EditWorkoutViewModel(Context context,
								FragmentManager fragmentManager,
								View hostView,
								WorkoutConfig workoutConfig) {
		this.context = context;
		this.fragmentManager = fragmentManager;
		this.hostView = hostView;
		this.workoutConfig = workoutConfig;
		this.isNewConfig = workoutConfig == null;
		if (isNewConfig) {
			recalculateTotalTime();
		} else {
			setInitialValues(workoutConfig);
		}
		disableLastRest.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
			@Override
			public void onPropertyChanged(Observable observable, int i) {
				recalculateTotalTime();
			}
		});
	}

	public rx.Observable<Boolean> getWorkoutSavedObservable() {
		return workoutSaved.asObservable();
	}

	private void setInitialValues(WorkoutConfig workoutConfig) {
		workoutName.set(workoutConfig.getName());
		disableLastRest.set(workoutConfig.isDisableLastRest());
		workSeconds = workoutConfig.getWorkSeconds();
		restSeconds = workoutConfig.getRestSeconds();
		warmupSeconds = workoutConfig.getWarmupSeconds();
		cooldownSeconds = workoutConfig.getCooldownSeconds();
		rounds = workoutConfig.getRounds();
		recalculateTotalTime();
	}

	private void recalculateTotalTime() {
		totalSeconds = disableLastRest.get() ? rounds * (workSeconds + restSeconds) - restSeconds + warmupSeconds + cooldownSeconds
				: rounds * (workSeconds + restSeconds) + warmupSeconds + cooldownSeconds;
		setTextValues();
	}

	private void setTextValues() {
		workTime.set(TimeFormatter.format(workSeconds));
		restTime.set(TimeFormatter.format(restSeconds));
		warmupTime.set(TimeFormatter.format(warmupSeconds));
		cooldownTime.set(TimeFormatter.format(cooldownSeconds));
		totalTime.set(TimeFormatter.format(totalSeconds));
		roundsText.set(String.valueOf(rounds));
	}


	public void onLayoutClick(View view) {
		String tag = view.getTag().toString();
		switch (tag) {
			case "rounds":
				showIntervalDialog(IntervalDialogFragment.DIALOG_ROUNDS_TYPE);
				break;
			case "work":
				showIntervalDialog(IntervalDialogFragment.DIALOG_WORK_TYPE);
				break;
			case "rest":
				showIntervalDialog(IntervalDialogFragment.DIALOG_REST_TYPE);
				break;
			case "warmup":
				showIntervalDialog(IntervalDialogFragment.DIALOG_WARMUP_TYPE);
				break;
			case "cooldown":
				showIntervalDialog(IntervalDialogFragment.DIALOG_COOLDOWN_TYPE);
				break;
			default:
				throw new IllegalStateException("invalid host layout tag");
		}
	}

	private void showIntervalDialog(int dialogType) {

		IntervalDialogFragment intervalDialogFragment = new IntervalDialogFragment();
		intervalDialogFragment.setListener(this);
		Bundle bundle = new Bundle();
		bundle.putInt(IntervalDialogFragment.TYPE, dialogType);

		switch (dialogType) {
			case IntervalDialogFragment.DIALOG_ROUNDS_TYPE:
				bundle.putInt(IntervalDialogFragment.ROUNDS, rounds);
				break;
			case IntervalDialogFragment.DIALOG_WORK_TYPE:
				bundle.putInt(IntervalDialogFragment.MINUTES, workSeconds / 60);
				bundle.putInt(IntervalDialogFragment.SECONDS, workSeconds % 60);
				break;
			case IntervalDialogFragment.DIALOG_REST_TYPE:
				bundle.putInt(IntervalDialogFragment.MINUTES, restSeconds / 60);
				bundle.putInt(IntervalDialogFragment.SECONDS, restSeconds % 60);
				break;
			case IntervalDialogFragment.DIALOG_WARMUP_TYPE:
				bundle.putInt(IntervalDialogFragment.MINUTES, warmupSeconds / 60);
				bundle.putInt(IntervalDialogFragment.SECONDS, warmupSeconds % 60);
				break;
			case IntervalDialogFragment.DIALOG_COOLDOWN_TYPE:
				bundle.putInt(IntervalDialogFragment.MINUTES, cooldownSeconds / 60);
				bundle.putInt(IntervalDialogFragment.SECONDS, cooldownSeconds % 60);
				break;
		}

		intervalDialogFragment.setArguments(bundle);
		intervalDialogFragment.show(fragmentManager, IntervalDialogFragment.TAG);
	}

	public void onSaveWorkoutClick(View view) {
		if (workoutName.get().isEmpty() || workoutName.get().equals(context.getString(R.string.new_profile_default_name))) {
			Snackbar.make(hostView, "Please input a workout name",
					Snackbar.LENGTH_SHORT).show();
			return;
		}
		DbAdapter dbAdapter = new DbAdapter(context);
		dbAdapter.open();
		if (isNewConfig) {
			if (checkIfNameExists(dbAdapter)) {
				return;
			}
			insertNewConfig(dbAdapter);
		} else {
			updateConfig(dbAdapter);
		}
		dbAdapter.close();
		workoutSaved.onNext(true);
	}

	private boolean checkIfNameExists(DbAdapter dbAdapter) {
		List<WorkoutConfig> workoutConfigs = dbAdapter.getAllConfigs();
		for (WorkoutConfig workoutConfig : workoutConfigs) {
			if (workoutConfig.getName().equalsIgnoreCase(workoutName.get())) {
				Snackbar.make(hostView, "Workout with that name already exists",
						Snackbar.LENGTH_SHORT).show();
				return true;
			}
		}
		return false;
	}

	private void updateConfig(DbAdapter dbAdapter) {
		dbAdapter.updateSetting(workoutConfig, rounds, workSeconds, restSeconds, warmupSeconds
				, cooldownSeconds, workoutName.get().trim(), disableLastRest.get());
	}

	private void insertNewConfig(DbAdapter dbAdapter) {
		WorkoutConfig.Builder builder = new WorkoutConfig.Builder()
				.rounds(rounds)
				.workSeconds(workSeconds)
				.restSeconds(restSeconds)
				.warmupSeconds(warmupSeconds)
				.cooldownSeconds(cooldownSeconds)
				.name(workoutName.get().trim())
				.disableLastRest(disableLastRest.get());

		dbAdapter.open();
		dbAdapter.insertNewConfig(builder.build());
		EventBus.getInstance().post(new ConfigCreatedEvent());
	}

	@Override
	public void onPositiveClick(DialogFragment dialogFragment, int dialogType, int totalSeconds) {
		switch (dialogType) {
			case IntervalDialogFragment.DIALOG_ROUNDS_TYPE:
				rounds = totalSeconds;
				break;
			case IntervalDialogFragment.DIALOG_WORK_TYPE:
				workSeconds = totalSeconds == 0 ? 5 : totalSeconds;
				break;
			case IntervalDialogFragment.DIALOG_REST_TYPE:
				restSeconds = totalSeconds == 0 ? 5 : totalSeconds;
				break;
			case IntervalDialogFragment.DIALOG_WARMUP_TYPE:
				warmupSeconds = totalSeconds;
				break;
			case IntervalDialogFragment.DIALOG_COOLDOWN_TYPE:
				cooldownSeconds = totalSeconds;
				break;
		}
		recalculateTotalTime();
	}
}