package markodevcic.hiitme.viewModels;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.Date;

import markodevcic.hiitme.controls.WorkoutProgressView;
import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.HistoryChangedEvent;
import markodevcic.hiitme.media.SoundPlayer;
import markodevcic.hiitme.media.VibrateWrapper;
import markodevcic.hiitme.utils.WorkoutStateColorResolver;
import markodevcic.hiitme.utils.TimeFormatter;
import markodevcic.hiitme.workout.History;
import markodevcic.hiitme.workout.Workout;
import markodevcic.hiitme.workout.WorkoutConfig;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

@SuppressWarnings("unused")
public final class WorkoutViewModel
		extends BaseObservable {

	public final ObservableInt roundProgress = new ObservableInt();
	public final ObservableInt workoutProgress = new ObservableInt();
	public final ObservableInt strokeColor = new ObservableInt();
	public final ObservableField<String> roundSeconds = new ObservableField<>();
	public final ObservableField<String> workoutSeconds = new ObservableField<>();
	public final ObservableField<String> currentRound = new ObservableField<>();
	public final ObservableField<String> workoutState = new ObservableField<>();
	public final ObservableField<String> totalRounds = new ObservableField<>();

	private final Workout workout;
	private final CompositeSubscription subscriptions = new CompositeSubscription();
	private final VibrateWrapper vibrateWrapper;
	private final SoundPlayer soundPlayer;
	private final WorkoutStateColorResolver colorResolver;
	private final DbAdapter dbAdapter;

	private final WorkoutConfig workoutConfig;

	private Context context;

	private Workout.Status workoutStatus = Workout.Status.STOPPED;

	private boolean keepScreenOn;
	private boolean isWorkoutPaused;

	public WorkoutViewModel(Context context, WorkoutConfig workoutConfig) {
		this.context = context;
		this.workoutConfig = workoutConfig;

		dbAdapter = new DbAdapter(context);
		colorResolver = new WorkoutStateColorResolver(context);
		workout = new Workout(workoutConfig);
		vibrateWrapper = new VibrateWrapper(context);
		soundPlayer = new SoundPlayer(context);

		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		keepScreenOn = sharedPreferences.getBoolean("pref_key_keep_screen_on", false);

		if (keepScreenOn) {
			((Activity)context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		createRoundProgressSubscription();
		createWorkoutProgressSubscription();
		createRoundsSecondsSubscription();
		createStateSubscription();
		createStatusSubscription();
		createWorkoutSecondsSubscription();
		createCurrentRoundsSubscription();
		createTotalRoundsSubscription();
	}

	public void start() {
		workout.start();
		soundPlayer.start();
	}

	public void stop() {
		workout.stop();
		soundPlayer.stop();
	}

	private void createStatusSubscription() {
		subscriptions.add(workout.getStatus()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<Workout.Status>() {
					@Override
					public void call(Workout.Status status) {
						workoutStatus = status;
						soundPlayer.setWorkoutStatus(status);
						if (status == Workout.Status.COMPLETED) {
							workoutCompleted();
						}
					}
				}));
	}

	private void workoutCompleted() {
		soundPlayer.workoutCompleted();
		insertHistory();
	}

	private void insertHistory() {
		History history = new History(new Date().getTime(), workoutConfig);
		dbAdapter.open();
		dbAdapter.insertHistory(history);
		dbAdapter.close();
		EventBus.getInstance().post(new HistoryChangedEvent());
	}

	private void createRoundsSecondsSubscription() {
		subscriptions.add(workout.getRoundSeconds()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<Integer>() {
					@Override
					public void call(Integer seconds) {
						roundSeconds.set(TimeFormatter.format(seconds));
					}
				}));
	}

	private void createWorkoutProgressSubscription() {
		subscriptions.add(workout.getWorkoutProgress()
				.observeOn(AndroidSchedulers.mainThread())
				.onBackpressureDrop()
				.retry()
				.subscribe(new Action1<Integer>() {
					@Override
					public void call(Integer progress) {
						workoutProgress.set(progress);
					}
				}, new Action1<Throwable>() {
					@Override
					public void call(Throwable throwable) {
						Log.e("Workout", throwable.getMessage(), throwable);
					}
				}));
	}

	private void createTotalRoundsSubscription() {
		subscriptions.add(workout.getTotalRounds()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<Integer>() {
					@Override
					public void call(Integer round) {
						totalRounds.set(round.toString());
					}
				}));
	}

	private void createCurrentRoundsSubscription() {
		subscriptions.add(workout.getCurrentRound()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<Integer>() {
					@Override
					public void call(Integer round) {
						currentRound.set(round.toString());
					}
				}));
	}

	private void createWorkoutSecondsSubscription() {
		subscriptions.add(workout.getWorkoutSeconds()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<Integer>() {
					@Override
					public void call(Integer seconds) {
						workoutSeconds.set(TimeFormatter.format(seconds));
					}
				}));
	}

	private void createStateSubscription() {
		subscriptions.add(workout.getState()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<String>() {
					@Override
					public void call(String state) {
						workoutState.set(state);
						soundPlayer.workoutStateChanged(state);
						vibrateWrapper.workoutStateChanged();
						strokeColor.set(colorResolver.getColor(state));
					}
				}));
	}

	private void createRoundProgressSubscription() {
		subscriptions.add(workout.getRoundProgress()
				.observeOn(AndroidSchedulers.mainThread())
				.onBackpressureDrop()
				.retry()
				.subscribe(new Action1<Integer>() {
					@Override
					public void call(Integer progress) {
						roundProgress.set(progress);
					}
				}, new Action1<Throwable>() {
					@Override
					public void call(Throwable throwable) {
						Log.e("Workout", throwable.getMessage(), throwable);
					}
				}));
	}

	public void onToggleVibrateClick(View view) {
		vibrateWrapper.toggleVibrate();
	}

	public void onToggleSoundClick(View view) {
		soundPlayer.toggleSoundTicks();
	}

	public void onToggleScreenClick(View view) {
		Window window = ((Activity) view.getContext()).getWindow();
		if (keepScreenOn) {
			keepScreenOn = false;
			window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			Toast.makeText(context, "Screen off", Toast.LENGTH_SHORT).show();
		} else {
			keepScreenOn = true;
			window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			Toast.makeText(context, "Keep screen on", Toast.LENGTH_SHORT).show();
		}
	}

	public void onToggleStatusClick(View view) {
		if (workoutStatus == Workout.Status.RUNNING) {
			workout.stop();
			soundPlayer.stop();
			isWorkoutPaused = true;
		} else {
			workout.start();
			soundPlayer.start();
			isWorkoutPaused = false;
		}
		((WorkoutProgressView) view).togglePause();
	}

	public void destroy() {
		subscriptions.unsubscribe();
		soundPlayer.release();
		context = null;
	}

	public Workout.Status getWorkoutStatus() {
		return workoutStatus;
	}

	public boolean isWorkoutPaused() {
		return isWorkoutPaused;
	}
}
