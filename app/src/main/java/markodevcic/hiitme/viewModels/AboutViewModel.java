package markodevcic.hiitme.viewModels;

import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.ObservableField;

public final class AboutViewModel {
	public final ObservableField<String> version = new ObservableField<>("");
	public final ObservableField<String> author = new ObservableField<>("");

	public AboutViewModel(Context context) {
		try {
			version.set("v" + context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		author.set("by Marko Devcic");
	}
}
