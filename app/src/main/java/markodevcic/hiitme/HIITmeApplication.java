package markodevcic.hiitme;

import android.app.Application;
import android.graphics.Typeface;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

public class HIITmeApplication extends Application {
	private Typeface robotoLightTypeFace;

	@Override
	public void onCreate() {
		super.onCreate();
		MultiDex.install(this);
		if (BuildConfig.DEBUG) {
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
					.detectAll()
					.penaltyLog()
					.build());
			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
					.detectAll()
					.penaltyLog()
					.build());
		}
	}

	public Typeface getRobotoLightTypeFace() {
		if (robotoLightTypeFace == null) {
			robotoLightTypeFace = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
		}
		return robotoLightTypeFace;
	}
}
