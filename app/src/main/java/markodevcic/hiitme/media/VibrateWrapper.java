package markodevcic.hiitme.media;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.widget.Toast;

public final class VibrateWrapper {
	private static final long VIBRATE_DURATION = 1000;

	private final Context context;

	private boolean isVibrateOn;

	public VibrateWrapper(Context context) {
		this.context = context;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		isVibrateOn = preferences.getBoolean("pref_vibrate", false);
	}

	public void toggleVibrate() {
		isVibrateOn = !isVibrateOn;
		Toast.makeText(context, "Vibrate on round end " + (isVibrateOn ? "on" : "off"),
				Toast.LENGTH_SHORT).show();
	}

	public void workoutStateChanged() {
		if (isVibrateOn) {
			Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATE_DURATION);
		}
	}
}