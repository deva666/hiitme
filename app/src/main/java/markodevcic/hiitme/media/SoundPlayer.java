package markodevcic.hiitme.media;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.Objects;

import markodevcic.hiitme.R;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.WorkoutCompletedEvent;
import markodevcic.hiitme.workout.Workout;

import static markodevcic.hiitme.utils.PreferenceHelper.PREF_PLAY_TICKS;
import static markodevcic.hiitme.utils.PreferenceHelper.PREF_SOUND;
import static markodevcic.hiitme.utils.PreferenceHelper.PREF_USE_DEFAULT_SOUND;

public final class SoundPlayer {
	private final Context context;
	private final boolean useDefaultSound;
	private final MediaPlayer mediaPlayer;
	private final String ringtoneSource;
	private final String resourceBase;

	private boolean playSoundTicks;
	private boolean isPlayingTicks;

	private String workoutState;
	private Workout.Status workoutStatus;

	public SoundPlayer(Context context) {
		this.context = context;
		resourceBase = "android.resource://" + context.getApplicationContext().getPackageName() + "/";
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		useDefaultSound = preferences.getBoolean(PREF_USE_DEFAULT_SOUND, true);
		playSoundTicks = preferences.getBoolean(PREF_PLAY_TICKS, true);
		ringtoneSource = preferences.getString(PREF_SOUND, "DEFAULT_SOUND");
		mediaPlayer = MediaPlayer.create(context, R.raw.whistle_2);
		mediaPlayer.setWakeMode(context, PowerManager.PARTIAL_WAKE_LOCK);
	}

	public void setWorkoutStatus(Workout.Status workoutStatus) {
		this.workoutStatus = workoutStatus;
	}

	public void start() {
		playSoundTicks(Objects.equals(workoutState, Workout.WORK_STATE));
	}

	public void stop() {
		mediaPlayer.stop();
		mediaPlayer.reset();
	}

	public void toggleSoundTicks() {
		playSoundTicks = !playSoundTicks;
		Toast.makeText(context, "Play sound ticks " + (playSoundTicks ? "on" : "off"), Toast
				.LENGTH_SHORT).show();
		if (workoutStatus != Workout.Status.RUNNING) {
			return;
		}
		if (isPlayingTicks && !playSoundTicks) {
			mediaPlayer.stop();
			mediaPlayer.reset();
		} else if (!mediaPlayer.isPlaying()) {
			playSoundTicks(Objects.equals(workoutState, Workout.WORK_STATE));
		}
	}

	private void playSoundTicks(boolean fast) {
		if (playSoundTicks) {
			try {
				mediaPlayer.stop();
				mediaPlayer.reset();
				mediaPlayer.setDataSource(context, Uri.parse(resourceBase +	(fast ? R.raw.fast_tick : R.raw.slow_tick)));
				mediaPlayer.prepare();
				mediaPlayer.setLooping(true);
				mediaPlayer.start();
				isPlayingTicks = true;
			} catch (IOException e) {
				Log.e("Media player", e.getMessage(), e);
			}
		}
	}

	public void release() {
		mediaPlayer.release();
	}

	public void workoutStateChanged(String state) {
		if (Objects.equals(state, Workout.WARMUP_STATE)) {
			return;
		}
		workoutState = state;
		if (mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
		mediaPlayer.reset();
		try {
			if (useDefaultSound) {
				mediaPlayer.setDataSource(context, Uri.parse(resourceBase + R.raw.whistle_2));
			} else {
				mediaPlayer.setDataSource(context, Uri.parse(ringtoneSource));
			}
			mediaPlayer.prepare();
			mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mPlayer) {
					playSoundTicks(Objects.equals(workoutState, Workout.WORK_STATE));
				}
			});
			mediaPlayer.start();
			isPlayingTicks = false;
		} catch (IOException e) {
			Log.d("Media player", e.getMessage());
		}
	}

	public void workoutCompleted() {
		try {
			mediaPlayer.stop();
			mediaPlayer.reset();
			mediaPlayer.setDataSource(context, Uri.parse(resourceBase + R.raw.whistle_finish));
			mediaPlayer.prepare();
			mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mediaPlayer) {
					mediaPlayer.reset();
					EventBus.getInstance().post(new WorkoutCompletedEvent());
				}
			});
			mediaPlayer.start();
			isPlayingTicks = false;
		} catch (IOException e) {
			Log.d("Media player", e.getMessage());
		}
	}
}