package markodevcic.hiitme.backup;

import android.app.Activity;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.GoogleDriveConnectionEvent;

public final class GoogleDriveService
		implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<DriveApi.DriveContentsResult> {

	public static final int GOOGLE_DRIVE_REQUEST_CODE = 1221;

	private static final String DB_FILE_NAME = "hiitme_backup.db";

	private final Activity activity;
	private final GoogleApiClient client;

	private boolean backupOnConnect = false;

	public GoogleDriveService(Activity activity) {
		this.activity = activity;
		client = new GoogleApiClient.Builder(activity)
				.addApi(Drive.API)
				.addScope(Drive.SCOPE_FILE)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();
	}

	public boolean isConnected() {
		return client.isConnected();
	}

	@Override
	public void onResult(@NonNull final DriveApi.DriveContentsResult result) {
		if (!result.getStatus().isSuccess()) {
			try {
				result.getStatus().startResolutionForResult(activity, GOOGLE_DRIVE_REQUEST_CODE);
			} catch (IntentSender.SendIntentException e) {
				Log.e(getClass().getSimpleName(), e.getMessage(), e);
			}
			return;
		}
		final DriveContents driveContents = result.getDriveContents();
		OutputStream outputStream = driveContents.getOutputStream();
		try {
			File dbFile = activity.getDatabasePath(DbAdapter.DATABASE_NAME);
			outputStream.write(readBytes(dbFile));
			outputStream.close();
		} catch (IOException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}

		MetadataChangeSet changeSet = getMetadataChangeSet();
		Drive.DriveApi.getRootFolder(client)
				.createFile(client, changeSet, driveContents)
				.setResultCallback(new ResultCallbacks<DriveFolder.DriveFileResult>() {
					@Override
					public void onSuccess(@NonNull DriveFolder.DriveFileResult driveFileResult) {
					}

					@Override
					public void onFailure(@NonNull Status status) {
					}
				});
	}

	private static byte[] readBytes(File file) throws IOException {
		int size = (int) file.length();
		byte bytes[] = new byte[size];
		byte tmpBuff[] = new byte[size];
		FileInputStream inputStream = new FileInputStream(file);
		try {
			int read = inputStream.read(bytes, 0, size);
			if (read < size) {
				int remain = size - read;
				while (remain > 0) {
					read = inputStream.read(tmpBuff, 0, remain);
					System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
					remain -= read;
				}
			}
		} catch (IOException e) {
			throw e;
		} finally {
			inputStream.close();
		}
		return bytes;
	}

	private static MetadataChangeSet getMetadataChangeSet() {
		String mimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType("db");
		return new MetadataChangeSet.Builder()
				.setTitle(DB_FILE_NAME)
				.setMimeType(mimeType)
				.setStarred(false)
				.build();
	}

	public void connect() {
		if (!client.isConnected() && !client.isConnecting()) {
			client.connect();
		}
	}

	public void connectAndBackup() {
		if (!client.isConnected() && !client.isConnecting()) {
			backupOnConnect = true;
			client.connect();
		} else {
			backupOnConnect = false;
			backup();
		}
	}

	public void disconnect() {
		if (client.isConnected()) {
			client.disconnect();
		}
	}

	@Override
	public void onConnected(@Nullable Bundle bundle) {
		Log.d(getClass().getSimpleName(), "Connected to Google Drive");
		if (backupOnConnect) {
			backupOnConnect = false;
			backup();
		} else {
			EventBus.getInstance().post(new GoogleDriveConnectionEvent(true));
		}
	}

	public void backup() {
		if (client.isConnected()) {
			doesDbFileExists(new ResultCallback<DriveApi.MetadataBufferResult>() {
				@Override
				public void onResult(@NonNull DriveApi.MetadataBufferResult result) {
					if (result.getStatus().isSuccess()) {
						boolean fileExists = false;
						for (Metadata metadata : result.getMetadataBuffer()) {
							fileExists = metadata.getTitle().equals(DB_FILE_NAME);
							if (fileExists) {
								deleteDbFile(metadata.getDriveId().encodeToString(), result);
								break;
							}
						}
						if (!fileExists) {
							result.release();
							startBackup();
						}
					}
				}
			});
		}
	}

	private void deleteDbFile(String fileId, @NonNull final Releasable result) {
		DriveFile driveFile = Drive.DriveApi.getFile(client, DriveId.decodeFromString(fileId));
		driveFile.delete(client)
				.setResultCallback(new ResultCallbacks<Status>() {
					@Override
					public void onSuccess(@NonNull Status status) {
						result.release();
						startBackup();
					}

					@Override
					public void onFailure(@NonNull Status status) {
						result.release();
					}
				});
	}

	private void startBackup() {
		Drive.DriveApi.newDriveContents(client)
				.setResultCallback(this);
	}

	public void restoreBackup(final RestoreDbCallback callback) {
		if (client.isConnected()) {
			doesDbFileExists(new ResultCallback<DriveApi.MetadataBufferResult>() {
				@Override
				public void onResult(@NonNull DriveApi.MetadataBufferResult result) {
					if (result.getStatus().isSuccess()) {
						boolean fileExists = false;
						for (Metadata metadata : result.getMetadataBuffer()) {
							fileExists = metadata.getTitle().equals(DB_FILE_NAME) && !metadata.isTrashed();
							if (fileExists) {
								startRestoreBackup(metadata, result, callback);
								break;
							}
						}
						if (!fileExists) {
							result.release();
							callback.onResult(false, false);
						}
					} else {
						result.release();
						callback.onResult(true, false);
					}
				}
			});
		} else {
			callback.onResult(false, false);
		}
	}

	private void doesDbFileExists(ResultCallback<DriveApi.MetadataBufferResult> resultResultCallback) {
		if (client.isConnected()) {
			Query query = new Query.Builder()
					.addFilter(Filters.and(Filters.eq(SearchableField.TITLE, DB_FILE_NAME), Filters.eq(SearchableField.TRASHED, false)))
					.build();
			Drive.DriveApi.query(client, query)
					.setResultCallback(resultResultCallback);
		}
	}

	private void startRestoreBackup(Metadata metadata, @NonNull final Releasable result, final RestoreDbCallback callback) {
		DriveFile driveFile = Drive.DriveApi.getFile(client, metadata.getDriveId());
		driveFile.open(client, DriveFile.MODE_READ_ONLY, null)
				.setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
					@Override
					public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
						if (driveContentsResult.getStatus().isSuccess()) {
							DriveContents driveContents = driveContentsResult.getDriveContents();
							File dbFile = activity.getDatabasePath(DbAdapter.DATABASE_NAME);
							boolean fileReplaced = false;
							try {
								if (dbFile.exists()) {
									dbFile.delete();
									InputStream inputStream = driveContents.getInputStream();
									OutputStream outputStream = new FileOutputStream(dbFile);
									byte[] buffer = new byte[1024];
									int bytesRead;
									while ((bytesRead = inputStream.read(buffer)) != -1) {
										outputStream.write(buffer, 0, bytesRead);
									}
									outputStream.close();
									inputStream.close();
									driveContents.discard(client);
									fileReplaced = true;
								}
							} catch (IOException e) {
								Log.e(GoogleDriveService.this.getClass().getSimpleName(), e.getMessage(), e);
							} finally {
								result.release();
								callback.onResult(true, fileReplaced);
							}
						} else {
							result.release();
							callback.onResult(false, false);
						}
					}
				});
	}

	@Override
	public void onConnectionSuspended(int i) {
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult result) {
		Log.d(getClass().getSimpleName(), "Connection failed");
		if (!result.hasResolution()) {
			GoogleApiAvailability.getInstance().getErrorDialog(activity, result.getErrorCode(), 0).show();
			return;
		}

		try {
			result.startResolutionForResult(activity, GOOGLE_DRIVE_REQUEST_CODE);
		} catch (IntentSender.SendIntentException e) {
			Log.e(getClass().getSimpleName(), "Exception while starting resolution activity", e);
		}
	}
}