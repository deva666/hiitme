package markodevcic.hiitme.backup;

public interface RestoreDbCallback {
	void onResult(boolean fileExists, boolean restoreSuccess);
}
