package markodevcic.hiitme.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import markodevcic.hiitme.HIITmeApplication;
import markodevcic.hiitme.R;
import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.HistoryChangedEvent;
import markodevcic.hiitme.events.InformUserEvent;
import markodevcic.hiitme.workout.History;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

public class StatisticsFragment extends Fragment
		implements Observer<List<History>> {
	private static final String USER_LEARNED_NAV = "user_learned_nav";

	private final EventBus eventBus = EventBus.getInstance();
	private final CompositeSubscription eventsSubscription = new CompositeSubscription();

	private View contentLayout;
	private TextView noHistoryText;

	private Subscription historySubscription = Subscriptions.unsubscribed();

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_statistics, container, false);
		initContent(rootView);
		contentLayout = rootView.findViewById(R.id.stats_content_layout);
		noHistoryText = (TextView) rootView.findViewById(R.id.stats_no_workouts);
		HIITmeApplication app = (HIITmeApplication) getActivity().getApplication();
		noHistoryText.setTypeface(app.getRobotoLightTypeFace());
		return rootView;
	}

	private void initContent(View view) {
		final ViewPager viewPager = (ViewPager) view.findViewById(R.id.statistics_pager);
		TabLayout tabLayout = (TabLayout) view.findViewById(R.id.statistics_tab_layout);
		FragmentStatePagerAdapter pagerAdapter = new StatsPagerAdapter(getChildFragmentManager());
		viewPager.setAdapter(pagerAdapter);
		tabLayout.setupWithViewPager(viewPager);
		final SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		boolean userInformed = sharedPreferences.getBoolean(USER_LEARNED_NAV, false);
		if (!userInformed) {
			viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				@Override
				public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

				}

				@Override
				public void onPageSelected(int position) {
					if (position == 1) {
						EventBus.getInstance().post(new InformUserEvent());
						sharedPreferences.edit().putBoolean(USER_LEARNED_NAV, true).apply();
					}
				}

				@Override
				public void onPageScrollStateChanged(int state) {

				}
			});
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		eventsSubscription.add(eventBus.subscribe(HistoryChangedEvent.class, new Action1<HistoryChangedEvent>() {
			@Override
			public void call(HistoryChangedEvent historyChangedEvent) {
				queryHistories();
			}
		}));
		queryHistories();
	}

	private void queryHistories() {
		historySubscription.unsubscribe();
		DbAdapter dbAdapter = new DbAdapter(getContext());
		historySubscription = dbAdapter.queryHistories()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		eventsSubscription.unsubscribe();
		historySubscription.unsubscribe();
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {

	}

	@Override
	public void onNext(List<History> histories) {
		if (!histories.isEmpty()) {
			noHistoryText.setVisibility(View.GONE);
			contentLayout.setVisibility(View.VISIBLE);
		} else {
			noHistoryText.setVisibility(View.VISIBLE);
			contentLayout.setVisibility(View.GONE);
		}
	}

	private static class StatsPagerAdapter extends FragmentStatePagerAdapter {

		public StatsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			switch (position) {
				case 0:
					return new HistoryListFragment();
				case 1:
					return new HistoryChartsFragment();
			}
			throw new IndexOutOfBoundsException("position");
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0:
					return "List";
				case 1:
					return "Chart";
			}
			throw new IndexOutOfBoundsException("position");
		}
	}
}