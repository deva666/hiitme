package markodevcic.hiitme.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import markodevcic.hiitme.HIITmeApplication;
import markodevcic.hiitme.R;
import markodevcic.hiitme.activity.EditWorkoutActivity;
import markodevcic.hiitme.activity.HomeActivity;
import markodevcic.hiitme.activity.StartupActivity;
import markodevcic.hiitme.activity.WorkoutActivity;
import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.events.BackupDbEvent;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.HistoryChangedEvent;
import markodevcic.hiitme.utils.TimeFormatter;
import markodevcic.hiitme.workout.History;
import markodevcic.hiitme.workout.WorkoutConfig;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

public class WorkoutsFragment
		extends Fragment
		implements Observer<List<WorkoutConfig>> {

	private RecyclerView recyclerView;
	private WorkoutsViewAdapter adapter;
	private Subscription configSubscription = Subscriptions.unsubscribed();


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_workouts, container, false);
		initViews(view);
		startConfigsQuery();
		return view;
	}

	private void initViews(View view) {
		recyclerView = (RecyclerView) view.findViewById(R.id.profiles_recycler_view);
		recyclerView.setHasFixedSize(false);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
		linearLayoutManager.setAutoMeasureEnabled(true);
		recyclerView.setLayoutManager(linearLayoutManager);
		FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.profiles_create);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showNewProfileActivity();
			}
		});
	}

	private void showNewProfileActivity() {
		Intent newProfileIntent = new Intent(getActivity(), EditWorkoutActivity.class);
		startActivityForResult(newProfileIntent, HomeActivity.NEW_PROFILE_ACTIVITY_RESULT);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		configSubscription.unsubscribe();
	}

	private void startConfigsQuery() {
		configSubscription.unsubscribe();
		DbAdapter dbAdapter = new DbAdapter(getContext());
		configSubscription = dbAdapter.queryConfigs()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this);
	}

	private void startEditWorkoutActivity(WorkoutConfig workoutConfig) {
		Intent intent = new Intent(getActivity(), EditWorkoutActivity.class);
		intent.putExtra(WorkoutActivity.EXTRA_WORKOUT_CONFIG, workoutConfig);
		startActivityForResult(intent, HomeActivity.NEW_PROFILE_ACTIVITY_RESULT);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == HomeActivity.NEW_PROFILE_ACTIVITY_RESULT && resultCode == EditWorkoutActivity.RESULT_WORKOUT_ADDED) {
			startConfigsQuery();
			EventBus.getInstance().post(new BackupDbEvent());
		} else if (requestCode == HomeActivity.NEW_WORKOUT_ACTIVITY_RESULT && resultCode == Activity.RESULT_OK) {
			showWorkoutCompleteMessage();
			EventBus.getInstance().post(new BackupDbEvent());
		}
	}


	private void showWorkoutCompleteMessage() {
		Snackbar.make(getView(), "Workout complete", Snackbar.LENGTH_LONG).show();
		startConfigsQuery();
	}

	private void deleteWorkout(WorkoutConfig config) {
		List<History> histories = config.getHistories();
		int historyCount = histories == null ? 0 : histories.size();
		if (historyCount > 0) {
			EventBus.getInstance().post(new HistoryChangedEvent());
		}
		DbAdapter dbAdapter = new DbAdapter(getContext());
		dbAdapter.open();
		dbAdapter.deleteConfig(config);
		int configCount = dbAdapter.getAllConfigs().size();
		dbAdapter.close();
		if (configCount == 0) {
			goToStartup();
		} else {
			startConfigsQuery();
			EventBus.getInstance().post(new BackupDbEvent());
		}
	}

	private void goToStartup() {
		Intent intent = new Intent(getActivity(), StartupActivity.class);
		startActivity(intent);
		getActivity().finish();
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {

	}

	@Override
	public void onNext(List<WorkoutConfig> workoutConfigs) {
		Collections.sort(workoutConfigs);
		fillRecyclerView(workoutConfigs);
	}

	private void fillRecyclerView(List<WorkoutConfig> configs) {
		adapter = new WorkoutsViewAdapter(configs);
		recyclerView.setAdapter(adapter);
	}

	private final class WorkoutsViewAdapter
			extends RecyclerView.Adapter
			implements PopupMenu.OnMenuItemClickListener {

		private final List<WorkoutConfig> workoutConfigs;

		private int menuPosition;

		public WorkoutsViewAdapter(List<WorkoutConfig> workoutConfigList) {
			this.workoutConfigs = workoutConfigList;
		}

		@Override
		public WorkoutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.item_profiles, parent, false);

			return new WorkoutViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
			final WorkoutViewHolder viewHolder = (WorkoutViewHolder) holder;

			final WorkoutConfig workoutConfig = workoutConfigs.get(position);
			List<History> historyList = workoutConfig.getHistories();

			viewHolder.btnStart.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					showWorkoutActivity(workoutConfig);
				}
			});
			viewHolder.textTitle.setText(workoutConfig.getName());
			viewHolder.textWarmup.setText(TimeFormatter.format(workoutConfig.getWarmupSeconds()));
			viewHolder.textWork.setText(TimeFormatter.format(workoutConfig.getWorkSeconds()));
			viewHolder.textRest.setText(TimeFormatter.format(workoutConfig.getRestSeconds()));
			viewHolder.textCooldown.setText(TimeFormatter.format(workoutConfig.getCooldownSeconds()));
			viewHolder.textTotalWorkouts.setText(historyList == null ? String.valueOf(0) :
					String.valueOf(historyList.size()));
			viewHolder.textTotalTime.setText(TimeFormatter.format(workoutConfig.getTotalTime()));
			viewHolder.textTotalRounds.setText(String.valueOf(workoutConfig.getRounds()));
			viewHolder.menu.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					menuPosition = viewHolder.getAdapterPosition();
					PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
					popupMenu.setOnMenuItemClickListener(WorkoutsViewAdapter.this);
					popupMenu.inflate(R.menu.profiles_menu);
					popupMenu.show();
				}
			});
		}

		private void showWorkoutActivity(WorkoutConfig config) {
			Intent intentWorkout = new Intent(getActivity(), WorkoutActivity.class);
			intentWorkout.putExtra(WorkoutActivity.EXTRA_WORKOUT_CONFIG, config);
			startActivityForResult(intentWorkout, HomeActivity.NEW_WORKOUT_ACTIVITY_RESULT);
		}

		@Override
		public int getItemCount() {
			return workoutConfigs.size();
		}

		@Override
		public boolean onMenuItemClick(MenuItem item) {
			if (item.getItemId() == R.id.menuitem_profile_delete) {
				WorkoutConfig config = workoutConfigs.get(menuPosition);
				showDeleteAlertDialog(config, menuPosition);
				return true;
			} else if (item.getItemId() == R.id.menuitem_profile_edit) {
				startEditWorkoutActivity(workoutConfigs.get(menuPosition));
				return true;
			}
			return false;
		}

		private void showDeleteAlertDialog(final WorkoutConfig config, final int menuPosition) {
			new AlertDialog.Builder(getActivity())
					.setTitle("Are you sure?")
					.setMessage("Workout is going to be deleted.")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							remove(menuPosition);
							deleteWorkout(config);
						}
					})
					.setNegativeButton("Cancel", null)
					.show();
		}

		private void remove(int position) {
			workoutConfigs.remove(position);
			adapter.notifyDataSetChanged();
		}

		private final class WorkoutViewHolder
				extends RecyclerView.ViewHolder {

			private final Button btnStart;
			private final TextView textTitle;
			private final TextView textWarmup;
			private final TextView textWork;
			private final TextView textRest;
			private final TextView textCooldown;
			private final TextView textTotalWorkouts;
			private final TextView textTotalTime;
			private final TextView textTotalRounds;
			private final ImageView menu;

			public WorkoutViewHolder(final View itemView) {
				super(itemView);
				btnStart = (Button) itemView.findViewById(R.id.profile_start);
				textTitle = (TextView) itemView.findViewById(R.id.profile_title);
				textWarmup = (TextView) itemView.findViewById(R.id.profile_warmup);
				textWork = (TextView) itemView.findViewById(R.id.profile_work);
				textRest = (TextView) itemView.findViewById(R.id.profile_rest);
				textCooldown = (TextView) itemView.findViewById(R.id.profile_cooldown);
				menu = (ImageView) itemView.findViewById(R.id.profile_menu);
				textTotalWorkouts = (TextView) itemView.findViewById(R.id.profile_total_workouts);
				textTotalTime = (TextView) itemView.findViewById(R.id.profile_total_time);
				textTotalRounds = (TextView) itemView.findViewById(R.id.profile_rounds);

				HIITmeApplication app = (HIITmeApplication) ((Activity) itemView.getContext()).getApplication();
				textTitle.setTypeface(app.getRobotoLightTypeFace());
			}
		}
	}
}