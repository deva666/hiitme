package markodevcic.hiitme.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class MessageDialogFragment extends DialogFragment {

    private DialogClickListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (listener == null) {
            try {
                listener = (DialogClickListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException("Activity " + activity.toString() + " must implement DialogClickListener Interface");
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString("message");
        String title = getArguments().getString("title");
        boolean okOnly = getArguments().getBoolean("okOnly");
        String positiveButton = getArguments().getString("positiveButton");
        String negativeButton = getArguments().getString("negativeButton");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        if (okOnly) {
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    listener.onPositiveClick(MessageDialogFragment.this);
                }
            });
        } else {
            builder.setPositiveButton(positiveButton == null ? "OK" : positiveButton, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    listener.onPositiveClick(MessageDialogFragment.this);
                }
            })
                    .setNegativeButton(negativeButton == null ? "Cancel" : negativeButton, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            listener.onNegativeClick(MessageDialogFragment.this);
                        }
                    });
        }
        Dialog dialog = builder.create();
        if (title != null) {
            dialog.setTitle(title);
        }
        return dialog;
    }

    public void setListener(DialogClickListener listener) {
        this.listener = listener;
    }

    public interface DialogClickListener {
        void onPositiveClick(DialogFragment dialogFragment);

        void onNegativeClick(DialogFragment dialogFragment);
    }
}
