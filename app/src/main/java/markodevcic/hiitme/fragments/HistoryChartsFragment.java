package markodevcic.hiitme.fragments;

import android.animation.Animator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import markodevcic.hiitme.R;
import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.HistoryChangedEvent;
import markodevcic.hiitme.events.InformUserEvent;
import markodevcic.hiitme.workout.History;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

public class HistoryChartsFragment extends Fragment
		implements OnChartGestureListener,
		Observer<List<History>> {

	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;

	private final EventBus eventBus = EventBus.getInstance();
	private final CompositeSubscription eventsSubscription = new CompositeSubscription();

	private int selectedMonth;
	private int selectedYear;

	private LineChart lineChart;
	private TextView monthText;
	private View root;

	private List<History> histories;
	private Subscription historySubscription = Subscriptions.unsubscribed();

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_history_charts, container, false);
		root = view.findViewById(R.id.chart_root);
		lineChart = (LineChart) view.findViewById(R.id.line_chart);
		monthText = (TextView) view.findViewById(R.id.month_text);
		DateTime now = new DateTime();
		selectedYear = now.getYear();
		selectedMonth = now.getMonthOfYear();
		monthText.setText(now.toString("MMMM-yy"));
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		eventsSubscription.add(eventBus.subscribe(HistoryChangedEvent.class, new Action1<HistoryChangedEvent>() {
			@Override
			public void call(HistoryChangedEvent historyChangedEvent) {
				queryHistories();
			}
		}));
		eventsSubscription.add(eventBus.subscribe(InformUserEvent.class, new Action1<InformUserEvent>() {
			@Override
			public void call(InformUserEvent informUserEvent) {
				informUser();
			}
		}));
		queryHistories();
	}

	private void queryHistories() {
		historySubscription.unsubscribe();
		DbAdapter dbAdapter = new DbAdapter(getContext());
		historySubscription = dbAdapter.queryHistories()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this);
	}

	private void informUser() {
		final Snackbar snackbar = Snackbar.make(root, "Swipe up or down on chart to " +
				"change displayed month", Snackbar.LENGTH_INDEFINITE);
		snackbar.setAction("Ok", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				snackbar.dismiss();
			}
		});
		snackbar.show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		eventsSubscription.unsubscribe();
		historySubscription.unsubscribe();
	}

	private void initChart(List<History> histories, int year, int month) {
		if (histories == null) {
			return;
		}
		LineData lineData = getLineData(histories, year, month);
		lineChart.setData(lineData);
		lineChart.getAxisLeft().setValueFormatter(new TimeFormatter());
		lineChart.getAxisLeft().setDrawGridLines(false);
		lineChart.getAxisRight().setEnabled(false);
		lineChart.setDrawGridBackground(false);
		lineChart.getLegend().setEnabled(false);
		lineChart.animateY(2000, Easing.EasingOption.EaseOutCubic);
		lineChart.setDescription("");
		lineChart.getXAxis().setDrawGridLines(false);
		lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
		lineChart.setOnChartGestureListener(this);
	}

	private LineData getLineData(List<History> histories, int year, int month) {
		Map<LocalDate, Integer> groupedHistories = groupByDate(histories, year, month);
		List<Entry> entries = new ArrayList<>(groupedHistories.size());
		int xAxisPosition = 0;
		for (Integer integer : groupedHistories.values()) {
			entries.add(new Entry(integer, xAxisPosition++));
		}

		LineDataSet dataSet = new LineDataSet(entries, "Workout times");
		dataSet.setHighlightEnabled(false);
		dataSet.setLineWidth(4f);
		dataSet.setDrawCircles(false);
		dataSet.setColor(getContext().getColor(R.color.dark_blue));
		List<String> axisLabels = new ArrayList<>();
		for (LocalDate localDate : groupedHistories.keySet()) {
			axisLabels.add(String.valueOf(localDate.getDayOfMonth()));
		}

		LineData data = new LineData(axisLabels, dataSet);
		data.setValueFormatter(new TimeFormatter());
		data.setDrawValues(false);
		data.setHighlightEnabled(false);
		return data;
	}

	private Map<LocalDate, Integer> groupByDate(List<History> histories, int year, int month) {
		Map<LocalDate, Integer> daysMap = new TreeMap<>();
		for (LocalDate day : getMonthDays(year, month)) {
			daysMap.put(day, 0);
		}

		for (History history : histories) {
			LocalDate localDate = history.getLocalDate();
			int workoutTime = history.getWorkoutConfig().getTotalTime();
			if (daysMap.containsKey(localDate)) {
				Integer time = daysMap.get(localDate);
				time += workoutTime;
				daysMap.put(localDate, time);
			}
		}

		return daysMap;
	}

	private List<LocalDate> getMonthDays(int year, int month) {
		List<LocalDate> dateList = new ArrayList<>();
		DateTime firstDayOfMonth = new DateTime(year, month, 1, 0, 0);
		LocalDate monthBegin = firstDayOfMonth.toLocalDate();
		for (int i = 0; i < daysOfMonth(year, month); i++) {
			dateList.add(monthBegin.plusDays(i));
		}
		return dateList;
	}

	private int daysOfMonth(int year, int month) {
		DateTime dateTime = new DateTime(year, month, 14, 12, 0, 0, 000);
		return dateTime.dayOfMonth().getMaximumValue();
	}

	private void fadeOutMonthText(final String month, final boolean down) {
		monthText.animate()
				.alpha(0)
				.translationY(down ? monthText.getHeight() : 0 - monthText.getHeight())
				.setDuration(250)
				.setListener(new Animator.AnimatorListener() {
					@Override
					public void onAnimationStart(Animator animation) {

					}

					@Override
					public void onAnimationEnd(Animator animation) {
						float height = monthText.getHeight();
						monthText.setY(down ? 0 - height : height);
						fadeInMonthText(month);
						monthText.animate().setListener(null);
					}

					@Override
					public void onAnimationCancel(Animator animation) {

					}

					@Override
					public void onAnimationRepeat(Animator animation) {

					}
				})
				.start();
	}

	private void fadeInMonthText(String month) {
		monthText.setText(month);
		monthText.animate()
				.alpha(1)
				.setInterpolator(new DecelerateInterpolator())
				.translationY(0)
				.setDuration(250)
				.start();
	}

	@Override
	public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

	}

	@Override
	public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
	}

	@Override
	public void onChartLongPressed(MotionEvent me) {

	}

	@Override
	public void onChartDoubleTapped(MotionEvent me) {

	}

	@Override
	public void onChartSingleTapped(MotionEvent me) {

	}

	@Override
	public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

		if (me1.getY() - me2.getY() > SWIPE_MIN_DISTANCE
				&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
			if (selectedMonth < 12) {
				selectedMonth++;
			} else {
				selectedMonth = 1;
				selectedYear++;
			}
			DateTime selectedDate = new DateTime(selectedYear, selectedMonth, 1, 0, 0);
			fadeOutMonthText(selectedDate.toString("MMMM-yy"), false);
			initChart(histories, selectedYear, selectedMonth);

		} else if (me2.getY() - me1.getY() > SWIPE_MIN_DISTANCE
				&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
			if (selectedMonth > 1) {
				selectedMonth--;
			} else {
				selectedMonth = 12;
				selectedYear--;
			}
			DateTime selectedDate = new DateTime(selectedYear, selectedMonth, 1, 0, 0);
			fadeOutMonthText(selectedDate.toString("MMMM-yy"), true);
			initChart(histories, selectedYear, selectedMonth);
		}
	}

	@Override
	public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

	}

	@Override
	public void onChartTranslate(MotionEvent me, float dX, float dY) {

	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {

	}

	@Override
	public void onNext(List<History> histories) {
		this.histories = histories;
		if (!histories.isEmpty()) {
			initChart(histories, selectedYear, selectedMonth);
		} else {
			lineChart.setNoDataText("No workouts so far");
		}
	}

	private static class TimeFormatter implements ValueFormatter, YAxisValueFormatter {

		@Override
		public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
			return markodevcic.hiitme.utils.TimeFormatter.format((int) value);
		}

		@Override
		public String getFormattedValue(float value, YAxis yAxis) {
			return markodevcic.hiitme.utils.TimeFormatter.format((int) value);
		}
	}
}