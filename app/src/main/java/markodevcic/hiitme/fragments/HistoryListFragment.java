package markodevcic.hiitme.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import markodevcic.hiitme.HIITmeApplication;
import markodevcic.hiitme.R;
import markodevcic.hiitme.controls.DividerItemDecoration;
import markodevcic.hiitme.data.DbAdapter;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.HistoryChangedEvent;
import markodevcic.hiitme.utils.TimeFormatter;
import markodevcic.hiitme.workout.History;
import markodevcic.hiitme.workout.WorkoutConfig;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

public class HistoryListFragment extends Fragment
		implements Observer<List<History>> {

	private final EventBus eventBus = EventBus.getInstance();
	private final CompositeSubscription eventsSubscription = new CompositeSubscription();

	private DbAdapter dbAdapter;
	private HistoryViewAdapter adapter;

	private Subscription historySubscription = Subscriptions.unsubscribed();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_history_list, container, false);
		dbAdapter = new DbAdapter(getActivity());
		adapter = new HistoryViewAdapter();
		RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.statistics_recycler_view);
		recyclerView.setHasFixedSize(false);
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		RecyclerView.ItemDecoration decoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
		recyclerView.addItemDecoration(decoration);
		recyclerView.setAdapter(adapter);
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		eventsSubscription.add(eventBus.subscribe(HistoryChangedEvent.class, new Action1<HistoryChangedEvent>() {
			@Override
			public void call(HistoryChangedEvent historyChangedEvent) {
				startHistoriesQuery();
			}
		}));
		startHistoriesQuery();
	}

	private void startHistoriesQuery() {
		adapter.clearItems();
		historySubscription = dbAdapter.startHistoriesQuery()
				.buffer(200, TimeUnit.MILLISECONDS)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		eventsSubscription.unsubscribe();
		historySubscription.unsubscribe();
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {
		Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
	}

	@Override
	public void onNext(List<History> history) {
		adapter.addItems(history);
	}

	private final static class HistoryViewAdapter extends RecyclerView.Adapter {

		private final List<History> historyList;

		public HistoryViewAdapter() {
			this.historyList = new ArrayList<>();
		}

		public void clearItems(){
			int size = historyList.size();
			historyList.clear();
			notifyItemRangeRemoved(0, size);
		}

		public void addItems(Collection<History> histories) {
			int oldSize = historyList.size();
			historyList.addAll(histories);
			int newSize = historyList.size();
			notifyItemRangeInserted(oldSize, newSize);
		}

		@Override
		public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.item_history, parent, false);

			return new HistoryViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
			HistoryViewHolder historyViewHolder = (HistoryViewHolder) holder;
			History history = historyList.get(position);
			DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
			String dateFormatted = dateFormat.format(new Date(history.getDateCompleted()));

			WorkoutConfig workoutConfig = history.getWorkoutConfig();

			historyViewHolder.textDate.setText(dateFormatted);
			historyViewHolder.textWorkoutName.setText(workoutConfig.getName());
			historyViewHolder.textTime.setText(TimeFormatter.format(workoutConfig.getTotalTime()));
		}

		@Override
		public int getItemCount() {
			return historyList.size();
		}

		private final static class HistoryViewHolder extends RecyclerView.ViewHolder {

			private final TextView textDate;
			private final TextView textTime;
			private final TextView textWorkoutName;

			public HistoryViewHolder(View itemView) {
				super(itemView);

				textDate = (TextView) itemView.findViewById(R.id.history_date);
				textTime = (TextView) itemView.findViewById(R.id.history_time);
				textWorkoutName = (TextView) itemView.findViewById(R.id.history_workout);

				HIITmeApplication app = (HIITmeApplication) ((Activity) itemView.getContext()).getApplication();
				textWorkoutName.setTypeface(app.getRobotoLightTypeFace());
			}
		}
	}
}