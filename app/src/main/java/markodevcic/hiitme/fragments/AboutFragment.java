package markodevcic.hiitme.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import markodevcic.hiitme.R;
import markodevcic.hiitme.databinding.FragmentAboutBinding;
import markodevcic.hiitme.viewModels.AboutViewModel;

public class AboutFragment extends Fragment {

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		FragmentAboutBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about, container, false);
		View rootView = binding.getRoot();
		binding.setViewModel(new AboutViewModel(getContext()));
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		((AppCompatActivity) getActivity()).getSupportActionBar().hide();
	}

	@Override
	public void onStop() {
		super.onStop();
		((AppCompatActivity) getActivity()).getSupportActionBar().show();
	}
}