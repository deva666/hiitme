package markodevcic.hiitme.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import markodevcic.hiitme.R;

public class IntervalDialogFragment extends DialogFragment {

	public static final int DIALOG_WORK_TYPE = 1;
	public static final int DIALOG_REST_TYPE = 2;
	public static final int DIALOG_WARMUP_TYPE = 3;
	public static final int DIALOG_COOLDOWN_TYPE = 4;
	public static final int DIALOG_ROUNDS_TYPE = 5;

	public static final String TYPE = "type";
	public static final String ROUNDS = "rounds";
	public static final String MINUTES = "mins";
	public static final String SECONDS = "secs";
	public static final String TAG = "intervalDialog";

	private int dialogType;
	private int rounds;
	private int minutes;
	private int seconds;

	private IntervalDialogListener listener;
	private NumberPicker numberPickerMins;
	private NumberPicker numberPickerSecs;
	private NumberPicker numberPickerRounds;

	public void setListener(IntervalDialogListener listener) {
		this.listener = listener;
	}

	@Override
	public void setArguments(Bundle args) {
		if (args.containsKey(TYPE))
			dialogType = args.getInt(TYPE);
		if (args.containsKey(ROUNDS))
			rounds = args.getInt(ROUNDS);
		if (args.containsKey(MINUTES))
			minutes = args.getInt(MINUTES);
		if (args.containsKey(SECONDS))
			seconds = args.getInt(SECONDS);
		super.setArguments(args);
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View view;
		TextView textViewTitle;
		if (dialogType == DIALOG_ROUNDS_TYPE) {
			view = layoutInflater.inflate(R.layout.dialog_rounds_pick, null);
			textViewTitle = (TextView) view.findViewById(R.id.dialog_rounds_textview_title);
			textViewTitle.setText("Set rounds");
			numberPickerRounds = (NumberPicker) view.findViewById(R.id.dialog_rounds_numberpicker);

			numberPickerRounds.setMaxValue(30);
			numberPickerRounds.setMinValue(1);
			numberPickerRounds.setValue(rounds == 0 ? 10 : rounds);

			builder.setView(view).setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					listener.onPositiveClick(IntervalDialogFragment.this, dialogType, numberPickerRounds.getValue());
				}
			});
		} else {
			view = layoutInflater.inflate(R.layout.dialog_interval_pick, null);

			textViewTitle = (TextView) view.findViewById(R.id.dialog_interval_textview_title);

			switch (dialogType) {
				case DIALOG_WORK_TYPE:
					textViewTitle.setText("Set work time");
					break;
				case DIALOG_REST_TYPE:
					textViewTitle.setText("Set rest time");
					break;
				case DIALOG_WARMUP_TYPE:
					textViewTitle.setText("Set warmup time");
					break;
				case DIALOG_COOLDOWN_TYPE:
					textViewTitle.setText("Set cooldown time");
					break;
			}

			numberPickerMins = (NumberPicker) view.findViewById(R.id.dialog_interval_numberpicker_mins);
			numberPickerSecs = (NumberPicker) view.findViewById(R.id.dialog_interval_numberpicker_secs);

			numberPickerMins.setMaxValue(59);
			numberPickerMins.setMinValue(0);
			numberPickerMins.setValue(minutes);

			numberPickerSecs.setMinValue(0);
			numberPickerSecs.setMaxValue(60);
			numberPickerSecs.setValue(seconds);

			builder.setView(view)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							listener.onPositiveClick(IntervalDialogFragment.this, dialogType, (numberPickerMins.getValue() * 60) + numberPickerSecs.getValue());
						}
					});
		}
		return builder.create();
	}

	public interface IntervalDialogListener {
		void onPositiveClick(DialogFragment dialogFragment, int dialogType, int totalSeconds);
	}
}
