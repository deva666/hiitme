package markodevcic.hiitme.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.support.design.widget.Snackbar;
import android.support.v4.preference.PreferenceFragment;
import android.view.View;

import java.util.Objects;

import markodevcic.hiitme.R;
import markodevcic.hiitme.activity.HomeActivity;
import markodevcic.hiitme.backup.RestoreDbCallback;
import markodevcic.hiitme.events.EventBus;
import markodevcic.hiitme.events.GoogleDriveConnectionEvent;
import markodevcic.hiitme.utils.PreferenceHelper;
import rx.Subscription;
import rx.functions.Action1;
import rx.subscriptions.Subscriptions;

public class SettingsFragment
		extends PreferenceFragment
		implements SharedPreferences.OnSharedPreferenceChangeListener {

	private Subscription eventSubscription = Subscriptions.unsubscribed();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.prefrences);
		initPreferences();
		eventSubscription = EventBus.getInstance().subscribe(GoogleDriveConnectionEvent.class, new Action1<GoogleDriveConnectionEvent>() {
			@Override
			public void call(GoogleDriveConnectionEvent googleDriveConnectionEvent) {
				if (googleDriveConnectionEvent.isConnected()) {
					Snackbar.make(getView(), "Google Drive connected", Snackbar.LENGTH_SHORT).show();
				} else {
					Snackbar.make(getView(), "Google Drive connection failed", Snackbar.LENGTH_SHORT).show();
					CheckBoxPreference backupDb = (CheckBoxPreference)findPreference(PreferenceHelper.PREF_BACKUP_DB);
					backupDb.setChecked(false);
				}
			}
		});
	}

	private void initPreferences() {
		Preference ringtonePref = findPreference(PreferenceHelper.PREF_SOUND);
		CheckBoxPreference useDefaultRingtone = (CheckBoxPreference) findPreference(PreferenceHelper.PREF_USE_DEFAULT_SOUND);
		ringtonePref.setEnabled(!useDefaultRingtone.isChecked());
	}

	@Override
	public void onPause() {
		super.onPause();
		PreferenceScreen screen = getPreferenceScreen();
		screen.getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (!eventSubscription.isUnsubscribed()) {
			eventSubscription.unsubscribe();
		}
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
		if (Objects.equals(preference.getKey(), PreferenceHelper.PREF_RESTORE_DB)) {
			if (!((HomeActivity) getActivity()).isGoogleDriveConnected()) {
				Snackbar.make(getView(), "Google Drive not connected", Snackbar.LENGTH_LONG)
						.setAction("connect", new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						((HomeActivity) getActivity()).connectToGoogleDrive();
					}
				}).show();
			} else {
			new AlertDialog.Builder(getActivity())
					.setTitle("Replace Database")
					.setMessage("If backup is found on Google Drive, existing data will be replaced with backed up data.")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							tryRestoreDb();
						}
					})
					.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					})
					.show();
		}}
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

	private void tryRestoreDb() {
		final ProgressDialog progressDialog = new ProgressDialog(getActivity());
		progressDialog.setIndeterminate(true);
		progressDialog.show();
		progressDialog.setCancelable(false);
		progressDialog.setMessage("Restoring ...");
		((HomeActivity)getActivity()).restoreBackup(new RestoreDbCallback() {
			@Override
			public void onResult(boolean fileExists, boolean restoreSuccess) {
				progressDialog.dismiss();
				if (!fileExists) {
					Snackbar.make(getView(), "No existing backups found", Snackbar.LENGTH_LONG).show();
				} else if (restoreSuccess) {
					Snackbar.make(getView(), "Database restored successfully", Snackbar.LENGTH_LONG).show();
				} else {
					Snackbar.make(getView(), "Error occurred while restoring the database", Snackbar.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		PreferenceScreen screen = getPreferenceScreen();
		screen.getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(PreferenceHelper.PREF_USE_DEFAULT_SOUND)) {
			initPreferences();
		} else if (key.equals(PreferenceHelper.PREF_BACKUP_DB)) {
			initPreferences();
			if (sharedPreferences.getBoolean(PreferenceHelper.PREF_BACKUP_DB, false)) {
				((HomeActivity) getActivity()).connectToGoogleDriveAndBackup();
			} else {
				((HomeActivity) getActivity()).disconnectGoogleDrive();
			}
		}
	}
}
