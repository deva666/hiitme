package markodevcic.hiitme.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import markodevcic.hiitme.activity.HomeActivity;
import markodevcic.hiitme.R;
import markodevcic.hiitme.databinding.FragmentCreateWorkoutBinding;
import markodevcic.hiitme.viewModels.EditWorkoutViewModel;
import rx.functions.Action1;

public class CreateWorkoutFragment extends Fragment {

	private EditWorkoutViewModel viewModel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		FragmentCreateWorkoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_workout, container, false);
		View rootView = binding.getRoot();
		viewModel = new EditWorkoutViewModel(getActivity(), getActivity().getSupportFragmentManager(),
				rootView.findViewById(R.id.startup_coordinator), null);
		viewModel.getWorkoutSavedObservable().subscribe(new Action1<Boolean>() {
			@Override
			public void call(Boolean aBoolean) {
				startHomeActivity();
			}
		});
		binding.setViewModel(viewModel);
		return rootView;
	}

	private void startHomeActivity() {
		Intent intent = new Intent(getActivity(), HomeActivity.class);
		startActivity(intent);
	}
}
