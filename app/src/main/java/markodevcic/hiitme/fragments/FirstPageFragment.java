package markodevcic.hiitme.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import markodevcic.hiitme.HIITmeApplication;
import markodevcic.hiitme.R;

public class FirstPageFragment extends Fragment {
    private RelativeLayout rootLayout;
    private Animation scaleAnimation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_landing, container, false);
        initViewsAndAnimation(rootView);
        return rootView;
    }

    private void initViewsAndAnimation(ViewGroup rootView) {
        scaleAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.scale);
        scaleAnimation.setInterpolator(new DecelerateInterpolator());

        rootLayout = (RelativeLayout) rootView.findViewById(R.id.startup_root);
        rootLayout.setAnimation(scaleAnimation);

        TextView textTitle = (TextView) rootView.findViewById(R.id.startup_title);
        TextView textMsg = (TextView) rootView.findViewById(R.id.startup_msg);

        HIITmeApplication app = (HIITmeApplication) getActivity().getApplication();
        textTitle.setTypeface(app.getRobotoLightTypeFace());
        textMsg.setTypeface(app.getRobotoLightTypeFace());
    }

    @Override
    public void onResume() {
        super.onResume();
        rootLayout.startAnimation(scaleAnimation);
    }
}
