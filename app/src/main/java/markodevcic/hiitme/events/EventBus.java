package markodevcic.hiitme.events;

import java.util.concurrent.atomic.AtomicReference;

import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

public final class EventBus {

	private static final AtomicReference<EventBus> INSTANCE = new AtomicReference<>(null);

	private final Subject<Event, Event> eventSubject = new SerializedSubject(PublishSubject.create());

	private EventBus() {

	}

	public static EventBus getInstance() {
		for (; ; ) {
			EventBus eventBus = INSTANCE.get();
			if (eventBus != null) {
				return eventBus;
			} else {
				eventBus = new EventBus();
				if (INSTANCE.compareAndSet(null, eventBus)) {
					return eventBus;
				}
			}
		}
	}

	public <T extends Event> void post(T event) {
		eventSubject.onNext(event);
	}

	public <T extends Event> Subscription subscribe(Class<T> eventClass, Action1<T> action) {
		return eventSubject.ofType(eventClass)
				.subscribe(action);
	}

	public <T extends Event> Subscription subscribe(Class<T> eventClass, Action1<T> action, Scheduler scheduler) {
		return eventSubject.ofType(eventClass)
				.subscribeOn(scheduler)
				.subscribe(action);
	}
}