package markodevcic.hiitme.events;

public class GoogleDriveConnectionEvent extends Event {

	private final boolean isConnected;

	public GoogleDriveConnectionEvent(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public boolean isConnected() {
		return isConnected;
	}
}
