# HIITme#

Android app for High Intensity Interval Training.
 
[Google play](https://play.google.com/store/apps/details?id=deva.hiitme&hl=en)

Written by [Marko Devcic](http://www.markodevcic.com)

### License
[MIT](https://opensource.org/licenses/MIT)